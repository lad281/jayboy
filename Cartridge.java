import java.io.*;

/**
 * this Class implements the Cartridge of a Gameboy.
 * it has different variables procedures to emulate the different kinds of Cartridges you can
 * find with GameBoy games.
 */
public class Cartridge
{
    /** Translation between ROM size byte contained in the ROM header, and the number
     *  of 16Kb ROM banks the cartridge will contain
     */
    private final int[][] romSizeTable = {{0, 2}, {1, 4}, {2, 8}, {3, 16}, {4, 32},
                {5, 64}, {6, 128}, {7, 256}, {0x52, 72}, {0x53, 80}, {0x54, 96}};
    
    /** Contains strings of the standard names of the cartridge mapper chips, indexed by
     *  cartridge type
     */
    private final String[] cartTypeTable =
        {"ROM Only",             /* 00 */
         "ROM+MBC1",             /* 01 */
         "ROM+MBC1+RAM",         /* 02 */
         "ROM+MBC1+RAM+BATTERY", /* 03 */
         "Unknown",              /* 04 */
         "ROM+MBC2",             /* 05 */
         "ROM+MBC2+BATTERY",     /* 06 */
         "Unknown",              /* 07 */
         "ROM+RAM",              /* 08 */
         "ROM+RAM+BATTERY",      /* 09 */
         "Unknown",              /* 0A */
         "Unsupported ROM+MMM01",/* 0B */
         "Unsupported ROM+MMM01+SRAM",             /* 0C */
         "Unsupported ROM+MMM01+SRAM+BATTERY",     /* 0D */
         "Unknown",              /* 0E */
         "ROM+MBC3+TIMER+BATTERY",     /* 0F */
         "ROM+MBC3+TIMER+RAM+BATTERY", /* 10 */
         "ROM+MBC3",             /* 11 */
         "ROM+MBC3+RAM",         /* 12 */
         "ROM+MBC3+RAM+BATTERY", /* 13 */
         "Unknown",              /* 14 */
         "Unknown",              /* 15 */
         "Unknown",              /* 16 */
         "Unknown",              /* 17 */
         "Unknown",              /* 18 */
         "ROM+MBC5",             /* 19 */
         "ROM+MBC5+RAM",         /* 1A */
         "ROM+MBC5+RAM+BATTERY", /* 1B */
         "ROM+MBC5+RUMBLE",      /* 1C */
         "ROM+MBC5+RUMBLE+RAM",  /* 1D */
         "ROM+MBC5+RUMBLE+RAM+BATTERY"  /* 1E */  };

    /** RTC Reg names */
    /**private final byte SECONDS = 0;
    private final byte MINUTES = 1;
    private final byte HOURS = 2;
    private final byte DAYS_LO = 3;
    private final byte DAYS_HI = 4;*/
    
    private byte[] rom; //contains the complete rom
    
    private byte[] ram = new byte[0x10000]; //contains the ram on the cartridge
    
    private int numBanks; //number of 16kb rom banks
    
    private int cartType; //type of cartridge
    
    private int pageStart; //Starting address of the ROM bank at 0x4000 in CPU address space
    private int currentBank = 1; //The bank number which is currently mapped at 0x4000 in CPU address space
    //The RAM bank number which is currently mapped at 0xA000 in CPU address space
    private int ramBank;
    private int ramPageStart;

    private boolean mbc1LargeRamMode = false;
    private boolean ramEnabled, disposed = false;
    
    private String romFileName; //filename of the currently loaded rom
    
    public boolean cartridgeReady = false;
    
    private boolean needsReset = false;

    private String romIntFileName;
    
    /**
     * create a cartridge object and load ROM
     */
    public Cartridge(String romFileName)
    {
        this.romFileName = romFileName; //setting romFileName
        InputStream is = null; //(input sream is used for reading byte based data)
        
        try
        {
            is = this.openRom(this.romFileName); //opens and loads rom file
            byte[] firstBank = new byte[0x04000]; //firstbank variable
            int total = 0x04000; //amount to read from the rom
            do
            {
               total -= is.read(firstBank, 0x04000 - total, total); // Read the first bank (bank 0)
            } while (total > 0);
            
            this.cartType = firstBank[0x0147]; //the cart type
            this.numBanks = lookUpCartSize(firstBank[0x0148]); //the number of 16kb rom banks
            this.rom = new byte[0x04000 * this.numBanks]; //recreate the rom array with the correct size
            // Copy first bank into main rom array
            for (int r = 0; r < 0x4000; r++) {
                rom[r] = firstBank[r];
            }
            
            total = 0x04000 * (this.numBanks - 1); // Calculate total ROM size (first one already loaded)
            do
            { // Read ROM into memory
                total -= is.read(rom, rom.length - total, total); // Read the entire ROM
            } while (total > 0);
            is.close();

            JayBoy.debugLog("Loaded ROM '" + this.romFileName + "'.  " + this.numBanks + " banks, " + (this.numBanks * 16) + "Kb.  " + getNumRAMBanks() + " RAM banks.");
            JayBoy.debugLog("Type: " + cartTypeTable[this.cartType] + " (" + JayBoy.hexByte(this.cartType) + ")");

            if (!verifyChecksum())
            {
                System.out.println("Warning This cartridge has an invalid checksum. It may not execute correctly.");
            }

            if (!JayBoy.getRunningAsApplet())
            {
                loadBatteryRam();
            }
            
            this.cartridgeReady = true; //the cartridge is now loaded and ready for emulation
        } catch (IOException e) {
            System.out.println("Error opening ROM image '" + this.romFileName + "'!");
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Error");
            System.out.println("Loading the ROM image failed.");
            System.out.println("The file is not a valid Gameboy ROM.");
        }
    }

    /**
     * update sneedsReset to enable the reset function
     */
    public void resetSystem()
    {
        this.needsReset= true;
    }
    
    /**
     * stripping the extension from the file name
     */
    String stripExtention(String filename)
    {
        int dotPosition = filename.lastIndexOf('.');
     
        if (dotPosition != -1)
        {
            return filename.substring(0, dotPosition);
        }
        else
        {
            return filename;
        }
    }

    /**
     * open ROM file and return it as an InputStream
     */
    public InputStream openRom(String romFileName)
    {
        //open plain gb file
        try
        {
            romIntFileName = stripExtention(romFileName);
            return new FileInputStream(new File(romFileName)); //returns an inputstream of the rom
        } catch (Exception e)
        {
            System.out.println("can't open file: "+e);
            return null;
        }
    }

    /**
     * Returns the byte currently mapped to a CPU address.
     * Addr must be in the range 0x0000 - 0x4000 or
     * 0xA000 - 0xB000 (for RAM access)
     */
    public final byte addressRead(int addr)
    {
        if ((addr >= 0xA000) && (addr <= 0xBFFF))
        {
            switch (this.cartType)
            {
                case 0x0F:
                case 0x10:
                case 0x11:
                case 0x12:
                case 0x13: 	/* MBC3 */
                default:
                {
                    return this.ram[addr - 0xA000 + this.ramPageStart];
                }
            }
        }
        if (addr < 0x4000)
        {
            return (byte) (this.rom[addr]);
        }
        else
        {
            return (byte) (this.rom[this.pageStart + addr - 0x4000]);
        }
    }

    /**
     * Writes to an address in CPU address space.
     * Writes to ROM may cause a mapping change.
     */
    public final void addressWrite(int addr, int data)
    {
        int ramAddress = 0;
    
        switch (this.cartType)
        {
            case 0 : /* ROM Only */
                break;
        
            case 1 : /* MBC1 */
            case 2 :
            case 3 :
            if ((addr >= 0xA000) && (addr <= 0xBFFF))
            {
                if (this.ramEnabled)
                {
                    ramAddress = addr - 0xA000 + this.ramPageStart;
                    this.ram[ramAddress] = (byte) data;
                }
            }
            if ((addr >= 0x2000) && (addr <= 0x3FFF))
            {
                int bankNo = data & 0x1F;
                if (bankNo == 0) bankNo = 1;
                mapRom((currentBank & 0x60) | bankNo);
            }
            else if ((addr >= 0x6000) && (addr <= 0x7FFF))
            {
                if ((data & 1) == 1)
                {
                    this.mbc1LargeRamMode = true;
                }
                else
                {
                    this.mbc1LargeRamMode = false;
                }
            }
            else if (addr <= 0x1FFF)
            {
                if ((data & 0x0F) == 0x0A)
                {
                    this.ramEnabled = true;
                }
                else
                {
                    this.ramEnabled = false;
                }
            }
            else if ((addr <= 0x5FFF) && (addr >= 0x4000))
            {
                if (this.mbc1LargeRamMode)
                {
                    this.ramBank = (data & 0x03);
                    this.ramPageStart = this.ramBank * 0x2000;
                }
                else
                {
                    mapRom((currentBank & 0x1F) | ((data & 0x03) << 5));
                }
            }
            break;
      
        case 5 :
        case 6 :
            if ((addr >= 0x2000) && (addr <= 0x3FFF) && ((addr & 0x0100) != 0) )
            {
                int bankNo = data & 0x1F;
                if (bankNo == 0) bankNo = 1;
                mapRom(bankNo);
            }
            if ((addr >= 0xA000) && (addr <= 0xBFFF))
            {
                if (this.ramEnabled)
                {
                    this.ram[addr - 0xA000 + this.ramPageStart] = (byte) data;
                }
            }
            break;
      
            case 0x0F :
            case 0x10 :
            case 0x11 :
            case 0x12 :
            case 0x13 :	/* MBC3 */
                // Select ROM bank
                if ((addr >= 0x2000) && (addr <= 0x3FFF))
                {
                    int bankNo = data & 0x7F;
                    if (bankNo == 0) bankNo = 1;
                    mapRom(bankNo);
                }
                else if ((addr <= 0x5FFF) && (addr >= 0x4000))
                {
                    // Select RAM bank
                    this.ramBank = data;
                
                    if (this.ramBank < 0x04)
                    {
                        this.ramPageStart = this.ramBank * 0x2000;
                    }
                } 
                if ((addr >= 0xA000) && (addr <= 0xBFFF))
                {
                    // Let the game write to RAM
                    if (this.ramBank <= 0x03)
                    {
                        this.ram[addr - 0xA000 + this.ramPageStart] = (byte) data;
                    }
                    else
                    {
                        // Write to realtime clock registers
                    }
      
                }
                break;
      
            case 0x19 :
            case 0x1A :
            case 0x1B :
            case 0x1C :
            case 0x1D :
            case 0x1E :
                if ((addr >= 0x2000) && (addr <= 0x2FFF))
                {
                    int bankNo = (currentBank & 0xFF00) | data;
                    mapRom(bankNo);
                }
                if ((addr >= 0x3000) && (addr <= 0x3FFF))
                {
                    int bankNo = (currentBank & 0x00FF) | ((data & 0x01) << 8);
                    mapRom(bankNo);
                }
      
                if ((addr >= 0x4000) && (addr <= 0x5FFF))
                {
                    this.ramBank = (data & 0x07);
                    this.ramPageStart = this.ramBank * 0x2000;
                }
                if ((addr >= 0xA000) && (addr <= 0xBFFF))
                {
                    this.ram[addr - 0xA000 + this.ramPageStart] = (byte) data;
                }
                break;
        }
    }

    /**
     * Maps a ROM bank into the CPU address space at 0x4000
     */
    public void mapRom(int bankNo)
    {
        currentBank = bankNo;
        this.pageStart = 0x4000 * bankNo;
    }

    public void reset()
    {
        mapRom(1); //resetting the bankNo to 1
    }

    /**
     * return the number of ram banks
     */
    public int getNumRAMBanks()
    {
        switch (this.rom[0x149])
        {
            case 0:
                return 0;
            case 1: 
            case 2:
                return 1;
            case 3:
                return 4;
            case 4:
                return 16;
        }
        return 0;
    }

    /** Read an image of battery RAM into memory if the current cartridge mapper supports it.
     *  The filename is the same as the ROM filename, but with a .SAV extention.
     *  Files are compatible with VGB-DOS.
     */
    public void loadBatteryRam()
    {
        String saveRamFileName = this.romFileName;
        int numRamBanks;
  
        try
        {
            int dotPosition = this.romFileName.lastIndexOf('.');
            if (dotPosition != -1) //if there is a dot in the string
            {
                saveRamFileName = this.romFileName.substring(0, dotPosition) + ".sav"; //create a new string but with .sav
            }
            else
            {
                saveRamFileName = this.romFileName + ".sav";
            }
  
            numRamBanks = getNumRAMBanks();
  
            //if the cartridge is caompatible with saves:
            if ((this.cartType == 3) || (this.cartType == 9) || (this.cartType == 0x1B) || (this.cartType == 0x1E) || (this.cartType == 0x10) || (this.cartType == 0x13) )
            {   //read the game into ram
                FileInputStream is = new FileInputStream(new File(saveRamFileName));
                is.read(this.ram, 0, numRamBanks * 8192);
                is.close();
                System.out.println("Read SRAM from '" + saveRamFileName + "'");
            }
            if (this.cartType == 6)
            {   //read the game into ram
                FileInputStream is = new FileInputStream(new File(saveRamFileName));
                is.read(this.ram, 0, 512);
                is.close();
                System.out.println("Read SRAM from '" + saveRamFileName + "'");
            }
        }
        catch (IOException e)
        {
            System.out.println("Error loading battery RAM from '" + saveRamFileName + "'");
        }
    }

    /**
     * return the battery ram size
     */
    public int getBatteryRamSize()
    {
        if (this.rom[0x149] == 0x06)
        {
            return 512;
        }
        else
        {
            return getNumRAMBanks() * 8192;
        }
    }

    /**
     * return the battery ram
     */
    public  byte[] getBatteryRam()
    {
        return this.ram;
    }

    /**
     * Writes an image of battery RAM to disk,
     * if the current cartridge mapper supports it.
     */
    public void saveBatteryRam()
    {
        String saveRamFileName = this.romFileName;
        int numRamBanks;
        numRamBanks = getNumRAMBanks();
  
        try
        {
            int dotPosition = this.romFileName.lastIndexOf('.');
            if (dotPosition != -1) //if there is a dot in the string
            {
                saveRamFileName = this.romFileName.substring(0, dotPosition) + ".sav"; //create a new string but with .sav
            }
            else
            {
                saveRamFileName = this.romFileName + ".sav"; //create a new string but with .sav
            }
  
            //if the cartridge is caompatible with saves:
            if ((this.cartType == 3) || (this.cartType == 9) || (this.cartType == 0x1B) || (this.cartType == 0x1E) || (this.cartType == 0x10) || (this.cartType == 0x13))
            {   //save ram to a new file
                FileOutputStream os = new FileOutputStream(new File(saveRamFileName));
                os.write(this.ram, 0, numRamBanks * 8192);
                os.close();
                System.out.println("Written SRAM to '" + saveRamFileName + "'");
            }
            if (this.cartType == 6)
            {   //save ram to a new file
                FileOutputStream os = new FileOutputStream(new File(saveRamFileName));
                os.write(this.ram, 0, 512);
                os.close();
                System.out.println("Written SRAM to '" + saveRamFileName + "'");
            }
        } catch (IOException e)
        {
            System.out.println("Error saving battery RAM to '" + saveRamFileName + "'");
        }
    }

    /**
     * Peforms saving of the battery RAM before the object is discarded
     */
    public void dispose()
    {
        saveBatteryRam();
        this.disposed = true;
    }

    /**
     * verifying the CheckSum of the rom
     * @return
     */
    public boolean verifyChecksum()
    {
        int checkSum = (JayBoy.unsign(this.rom[0x14E]) << 8) + JayBoy.unsign(this.rom[0x14F]);
      
        int total = 0;     // Calculate ROM checksum
        for (int r=0; r < this.rom.length; r++)
        {
            if ((r != 0x14E) && (r != 0x14F))
            {
                total = (total + JayBoy.unsign(this.rom[r])) & 0x0000FFFF;
            }
        }
        return checkSum == total;
    }
    
    /**
     * Returns the number of 16Kb banks in a cartridge from the header size byte.
     */
    public int lookUpCartSize(int sizeByte)
    {
        int i = 0;
        while ((i < this.romSizeTable.length) && (this.romSizeTable[i][0] != sizeByte))
        {
            i++;
        }

        if (this.romSizeTable[i][0] == sizeByte)
        {
            return this.romSizeTable[i][1];
        } else {
            return -1;
        }
    }
}