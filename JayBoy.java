import java.io.*;
import java.awt.event.KeyListener;
import java.awt.event.WindowListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.util.StringTokenizer;


/** This is the main controlling class which contains the main() method
 *  to run JayBoy as an application, and also the necessary applet methods.
 *  It also implements a full command based debugger using the console.
 */

public class JayBoy extends java.applet.Applet implements Runnable, KeyListener, WindowListener
{
    private final int WIDTH = 160; //width of app GB screen resolution
    private final int HEIGHT = 144; //height of app GB screen resolution
    
    private static final String hexChars = "0123456789ABCDEF";
    
    //The version string is displayed on the title bar of the application
    private static String versionString = "0.8";
    
    private boolean appletRunning = true; //is the applet running
    private static boolean runningAsApplet; //is it running as applet
    private boolean fullFrame = true;
    
    
    /** This array contains the actual data for the colour schemes.
     *  These are only using in DMG mode.
     *  The first four values control the BG palette, the second four
     *  are the OBJ0 palette, and the third set of four are OBJ1.
     */
    static public int[] schemeColours =
      {0xFFFFFFFF, 0xFFAAAAAA, 0xFF555555, 0xFF000000,
        0xFFFFFFFF, 0xFFAAAAAA, 0xFF555555, 0xFF000000,
        0xFFFFFFFF, 0xFFAAAAAA, 0xFF555555, 0xFF000000};
    
        
    public Cartridge cartridge; //current cartridge
    public Cpu cpu; //current cpu

    /** When running as an application, contains a reference to the interface frame object */
    private GameBoyScreen mainWindow;

    /** Stores commands queued to be executed by the debugger */
    private String debuggerQueue = null;

    /** True when the commands in debuggerQueue have yet to be executed */
    private boolean debuggerPending = false;

    /** True when the debugger console interface is active */
    private boolean debuggerActive = false;

    /** Contols Keyboard keys
     * z=A
     * x=B
     * c=START
     * v=SELECT
     * UP ARROW=UP
     * DOWN ARROW=DOWN
     * LEFT ARROW=LEFT
     * RIGHT ARROW=RIGHT
     */
    public static int[] keyCodes = {38, 40, 37, 39, 90, 88, 67, 86};

    private boolean keyListener = false;
    
    /**
     * Returns the unsigned value (0 - 255) of a signed byte
     */
    public static short unsign(byte b)
    {
        if (b < 0)
        {
            return (short) (256 + b);
        }
        else
        {
            return b;
        }
    }
  
   /**
    * Returns the unsigned value (0 - 255) of a signed 8-bit value stored in a short
    */
    public static short unsign(short b)
    {
        if (b < 0)
        {
            return (short) (256 + b);
        }
        else
        {
            return b;
        }
    }

    /**
     * Returns a string representation of an 8-bit number in hexadecimal
     */
    static public String hexByte(int b)
    {
        String s = Character.valueOf(hexChars.charAt(b >> 4)).toString();
        s = s + Character.valueOf(hexChars.charAt(b & 0x0F)).toString();
        return s;
    }
  
    /**
     * Returns a string representation of an 16-bit number in hexadecimal
     */
    static public String hexWord(int w)
    {
        return new String(hexByte((w & 0x0000FF00) >>  8) + hexByte(w & 0x000000FF));
    }

    /**
     * output a debug log to the console
     */
    public static void debugLog(String log)
    {
        System.out.println(log);
    }
       
    public void drawNextFrame()
    {
        this.fullFrame = false;
        repaint();
    }

    public void keyTyped(KeyEvent e) {
    }
   
    /**
     * handles pressing of keys
     * @param e
     */
    public void keyPressed(KeyEvent e)
    {
        int key = e.getKeyCode(); //return the int code of the key
        
        if (key == keyCodes[0]) //up pad
        {
          this.cpu.ioHandler.padUp = true;
          this.cpu.triggerInterruptIfEnabled(this.cpu.INT_P10); //joypad interrupt
        }
        else if (key == keyCodes[1]) //down pad
        {
          this.cpu.ioHandler.padDown = true;
          this.cpu.triggerInterruptIfEnabled(this.cpu.INT_P10); //joypad interrupt
        }
        else if (key == keyCodes[2]) //left pad
        {
          this.cpu.ioHandler.padLeft = true;
          this.cpu.triggerInterruptIfEnabled(this.cpu.INT_P10); //joypad interrupt
        }
        else if (key == keyCodes[3]) //right pad
        {
          this.cpu.ioHandler.padRight = true;
          this.cpu.triggerInterruptIfEnabled(this.cpu.INT_P10); //joypad interrupt
        }
        else if (key == keyCodes[4]) //A pad
        {
          this.cpu.ioHandler.padA = true;
          this.cpu.triggerInterruptIfEnabled(this.cpu.INT_P10); //joypad interrupt
        }
        else if (key == keyCodes[5]) //B pad
        {
          this.cpu.ioHandler.padB = true;
          this.cpu.triggerInterruptIfEnabled(this.cpu.INT_P10); //joypad interrupt
        }
        else if (key == keyCodes[6]) //START pad
        {
          this.cpu.ioHandler.padStart = true;
          this.cpu.triggerInterruptIfEnabled(this.cpu.INT_P10); //joypad interrupt
        }
        else if (key == keyCodes[7]) //SELECT pad
        {
          this.cpu.ioHandler.padSelect = true;
          this.cpu.triggerInterruptIfEnabled(this.cpu.INT_P10); //joypad interrupt
        }
    }
   
    /**
     * handles releases of keys
     * @param e
     */
    public void keyReleased(KeyEvent e)
    {
        int key = e.getKeyCode(); //return the int code of the key
        
        if (key == keyCodes[0]) //up pad
        {
            this.cpu.ioHandler.padUp = false;
            this.cpu.triggerInterruptIfEnabled(this.cpu.INT_P10); //joypad interrupt
        }
        else if (key == keyCodes[1]) //down pad
        {
            this.cpu.ioHandler.padDown = false;
            this.cpu.triggerInterruptIfEnabled(this.cpu.INT_P10); //joypad interrupt
        }
        else if (key == keyCodes[2]) //left pad
        {
            this.cpu.ioHandler.padLeft = false;
            this.cpu.triggerInterruptIfEnabled(this.cpu.INT_P10); //joypad interrupt
        }
        else if (key == keyCodes[3]) //right pad
        {
            this.cpu.ioHandler.padRight = false;
            this.cpu.triggerInterruptIfEnabled(this.cpu.INT_P10); //joypad interrupt
        }
        else if (key == keyCodes[4]) //A pad
        {
            this.cpu.ioHandler.padA = false;
            this.cpu.triggerInterruptIfEnabled(this.cpu.INT_P10); //joypad interrupt
        }
        else if (key == keyCodes[5]) //B pad
        {
            this.cpu.ioHandler.padB = false;
            this.cpu.triggerInterruptIfEnabled(this.cpu.INT_P10); //joypad interrupt
        }
        else if (key == keyCodes[6]) //START pad
        {
            this.cpu.ioHandler.padStart = false;
            this.cpu.triggerInterruptIfEnabled(this.cpu.INT_P10); //joypad interrupt
        }
        else if (key == keyCodes[7]) //SELECT pad
        {
            this.cpu.ioHandler.padSelect = false;
            this.cpu.triggerInterruptIfEnabled(this.cpu.INT_P10); //joypad interrupt
        }
    }

    /**
     * setting up the keyboard
     */
    public void setupKeyboard()
    {
        if (!keyListener)
        {
            System.out.println("Starting key controls");
            this.mainWindow.addKeyListener(this);
            this.mainWindow.requestFocus();
            this.keyListener = true; //the app is now listening to keyboard input
        }
    }


    public JayBoy()
    {}
   
    /** Initialize JayBoy when run as an application */
    public JayBoy(String cartName)
    {
        this.mainWindow = new GameBoyScreen("JayBoy " + versionString, this); //creating a new window
        this.mainWindow.setVisible(true);
        this.requestFocus();
        this.mainWindow.addWindowListener(this);
        }

    public static void main(String[] args)
    {
        System.out.println("JayBoy Version " + versionString);
        JayBoy jayBoy = new JayBoy(""); //create a new JayBoy
      
        Thread p = new Thread(jayBoy); //create a new thread
        p.start(); //calls run()
    }
    
    public void run()
    {
        if (this.runningAsApplet)
        {
            System.out.println("Starting...");
            do
            {
                this.cpu.reset();
                this.cpu.execute(-1); //forever
            } while (this.appletRunning);
        }
      
        do
        {
            try
            {
                getDebuggerMenuChoice(); //debugger execution
                java.lang.Thread.sleep(1);
                this.requestFocus();
            } catch (InterruptedException e)
            {
                System.out.println("Interrupted!");
                break;
            }
        } while (this.appletRunning);
        dispose();
        System.out.println("Thread terminated");
    }
      
    /** Free up allocated memory */
    public void dispose()
    {
        if (this.cartridge != null)
        {
            this.cartridge.dispose(); //dispose of cartridge data
        }
        if (this.cpu != null)
        {
            this.cpu.dispose(); //"reset" the cpu
        }
    }

    /** Output a debugger command list to the console */
    public void displayDebuggerHelp()
    {
        System.out.println("Enter a command followed by it's parameters (all values in hex):");
        System.out.println("?                     Display this help screen");
        System.out.println("s                     Reset CPU");
        System.out.println("g                     Execute forever");
        System.out.println("o                     Output Gameboy screen to applet window");
        System.out.println("k [keyname]           Toggle Gameboy key");
        System.out.println("q                     Quit debugger interface");
        System.out.println("<CTRL> + C            Quit JayBoy");
   }
  
    /** Execute any pending debugger commands, or get a command from the console and execute it */
    public void getDebuggerMenuChoice()
    {
        String command = new String("");
        char b = 0;
        if (this.cpu != null) this.cpu.terminate = false;
        
        if (!this.debuggerActive)
        {
            if (this.debuggerPending)
            {
                this.debuggerPending = false;
                executeDebuggerCommand(this.debuggerQueue); //execute the debbugger queue
            }
        }
        else
        {
            System.out.println();
            System.out.print("Enter command ('?' for help)> ");

            while ((b != 10) && (this.appletRunning))
            {
                try
                {
                    b = (char) System.in.read();
                } catch (IOException e)
                {
                }
                if (b >= 32) command = command + b;
            }
        }
        if (this.appletRunning) executeDebuggerCommand(command);
    }
  
   
    /** Queue a debugger command for later execution */
    public void queueDebuggerCommand(String command)
    {
        this.debuggerQueue = command;
        this.debuggerPending = true;
    }
   
    /** Execute a debugger command which can consist of many commands separated by semicolons */
    public void executeDebuggerCommand(String commands)
    {
        StringTokenizer commandTokens = new StringTokenizer(commands, ";");
    
        while (commandTokens.hasMoreTokens())
        {
            executeSingleDebuggerCommand(commandTokens.nextToken()); //execute each command in the given commands
        }
    }
  
    /** Execute a single debugger command */
    public void executeSingleDebuggerCommand(String command)
    {
        StringTokenizer st = new StringTokenizer(command, " \n");
        
        try
        {
            switch (st.nextToken().charAt(0))
            {
                case '?' : //display help
                     displayDebuggerHelp();
                     break;
                case 'k' : //key handler
                     try
                     {
                        String keyName = st.nextToken();
                        this.cpu.ioHandler.toggleKey(keyName);
                     } catch (java.util.NoSuchElementException e) {
                        System.out.println("Invalid number of parameters to 'k' command.");
                     }
                     break;
                case 's' : //reset the cpu
                     System.out.println("- CPU Reset");
                     this.cpu.reset();
                     break;
                case 'o' : //repaint
                     repaint();
                     break;
                case 'q' : //quit debugger
                     System.out.println("- Quitting debugger");
                     deactivateDebugger();
                     break;
                case 'g' : //execute forever
                     setupKeyboard();
                     this.cpu.execute(-1);
                     break;
                default :
                     System.out.println("Command not recognized.  Try looking at the help page.");
            }
        } catch (java.util.NoSuchElementException e)
        {
         // Do nothing
        }
    
    }

    /** Deactivate the console debugger interface */
    public void deactivateDebugger()
    {
        this.debuggerActive = false;
    }

    public boolean getDebuggerActive()
    {
        return this.debuggerActive;
    }

    public void setDebuggerActive(boolean debugActive)
    {
        this.debuggerActive = debugActive;
    }

    public static boolean getRunningAsApplet()
    {
        return runningAsApplet;
    }

   @Override
   public void windowOpened(WindowEvent e) {
       // TODO Auto-generated method stub

   }

   @Override
   /**
    * dispose of stuff and save SRam when pressing X (closing the app)
    */
   public void windowClosing(WindowEvent e) {
    dispose();
    System.exit(0);

   }

   @Override
   public void windowClosed(WindowEvent e) {
       // TODO Auto-generated method stub

   }

   @Override
   public void windowIconified(WindowEvent e) {
       // TODO Auto-generated method stub

   }

   @Override
   public void windowDeiconified(WindowEvent e) {
       // TODO Auto-generated method stub

   }

   @Override
   public void windowActivated(WindowEvent e) {
       // TODO Auto-generated method stub

   }

   @Override
   public void windowDeactivated(WindowEvent e) {
       // TODO Auto-generated method stub

   }
}