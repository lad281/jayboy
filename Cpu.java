import java.awt.*;

/** This is the main controlling class for the emulation
 *  It contains the code to emulate the Z80-like processor
 *  found in the Gameboy, and code to provide the locations
 *  in CPU address space that points to the correct area of
 *  ROM/RAM/IO.
 */
class Cpu
{
    /** Registers: 8-bit */
    private int a, b, c, d, e, f;
    /** Registers: 16-bit */
    private int sp, pc, hl;

    /** The number of instructions that have been executed since the
     *  last reset
     */
    private int instrCount = 0;

    private boolean interruptsEnabled = false;

    /** Used to implement the IE delay slot */
    private int ieDelay = -1;

    boolean timaEnabled = false;
    int instrsPerTima = 6000;

    /** TRUE when the CPU is currently processing an interrupt */
    private boolean inInterrupt = false;

    /** Enable the breakpoint flag.  As breakpoint instruction is used in some games, this is used to skip over it unless the breakpoint is actually in use */
    private boolean breakpointEnable = false;

    // Constants for flags register

    /** Zero flag */
    private final short F_ZERO =      0x80;
    /** Subtract/negative flag */
    private final short F_SUBTRACT =  0x40;
    /** Half carry flag */
    private final short F_HALFCARRY = 0x20;
    /** Carry flag */
    private final short F_CARRY =     0x10;

    final short INSTRS_PER_VBLANK = 9000; /* 10000  */

    /** Used to set the speed of the emulator.  This controls how
     *  many instructions are executed for each horizontal line scanned
     *  on the screen.  Multiply by 154 to find out how many instructions
     *  per frame.
     */
    private final short BASE_INSTRS_PER_HBLANK = 60;    /* 60    */
    short INSTRS_PER_HBLANK = BASE_INSTRS_PER_HBLANK;

    /** Used to set the speed of DIV increments */
    private final short BASE_INSTRS_PER_DIV    = 33;    /* 33    */
    private short INSTRS_PER_DIV = BASE_INSTRS_PER_DIV;

    // Constants for interrupts

    /** Vertical blank interrupt */
    final short INT_VBLANK =  0x01;

    /** LCD Coincidence interrupt */
    private final short INT_LCDC =    0x02;

    /** TIMA (programmable timer) interrupt */
    private final short INT_TIMA =    0x04;

    /** Serial interrupt */
    final short INT_SER =     0x08;

    /** P10 - P13 (Joypad) interrupt */
    public final short INT_P10 =     0x10;

    // 8Kb main system RAM appears at 0xC000 in address space
    private byte[] mainRam = new byte[0x8000];

    // 256 bytes at top of RAM are used mainly for registers
    //operations administration and management
    private byte[] oam = new byte[0x100];

    Cartridge cartridge; //referencing JayBoy's cartridge
    GraphicsChip graphicsChip; //current graphicsChip
    IoHandler ioHandler; //current IOHandler
    private Component applet; //referencing JayBoy as Component
    boolean terminate; //terminate cpu
    boolean running = false; //is the cpu running

    boolean gbcFeatures = false; //use gbc features?
    private int gbcRamBank = 1;

    /** Create a CPU emulator with the supplied cartridge.  It can be set up
     *  or changed later if needed
     */
    public Cpu(Cartridge c, Component a)
    {
        this.cartridge = c; //setting up a new cartridge
        this.graphicsChip = new TileBasedGraphicsChip(a, this); //setting up a graphics chip

        this.ioHandler = new IoHandler(this); //setting up a new IOHandler
        this.applet = a; //linking to JayBoy class
    }

    /** Clear up memory */
    public void dispose()
    {
       this.graphicsChip.dispose();
    }

    /** Force the execution thread to stop and return to it's caller */
    public void terminateProcess()    
    {
       this.terminate = true;
    }

    public int getInstrCount()
    {
        return this.instrCount;
    }

    /** Perform a CPU address space read.  This maps all the relevant objects into the correct parts of
     *  the memory
     */
    public final short addressRead(int addr)
    {
        addr = addr & 0xFFFF; //enable everything

        switch ((addr & 0xF000))
        {
            case 0x0000 :
            case 0x1000 :
            case 0x2000 :
            case 0x3000 :
            case 0x4000 :
            case 0x5000 :
            case 0x6000 :
            case 0x7000 :
             return this.cartridge.addressRead(addr);
            
            case 0x8000 :
            case 0x9000 :
             return this.graphicsChip.addressRead(addr - 0x8000);
            
            case 0xA000 :
            case 0xB000 :
             return this.cartridge.addressRead(addr);
            
            case 0xC000 :
             return (this.mainRam[addr - 0xC000]);
            
            case 0xD000 :
             return (this.mainRam[addr - 0xD000 + (gbcRamBank * 0x1000)]);
            
            case 0xE000 :
             return this.mainRam[addr - 0xE000];
            
            case 0xF000 :
             if (addr < 0xFE00)
             {
                return this.mainRam[addr - 0xE000];
             }
             else if (addr < 0xFF00)
             {
                return (short) (this.oam[addr - 0xFE00] & 0x00FF);
             }
             else
             {
                return this.ioHandler.ioRead(addr - 0xFF00);
             }
          
            default:
             System.out.println("Tried to read address " + addr + ".  pc = " + JayBoy.hexWord(this.pc));
             return 0xFF;
        }
    }

    /** Performs a CPU address space write.  Maps all of the relevant object into the right parts of
     *  memory.
     */
    public final void addressWrite(int addr, int data) 
    {
        switch (addr & 0xF000)
        {
            case 0x0000 :
            case 0x1000 :
            case 0x2000 :
            case 0x3000 :
            case 0x4000 :
            case 0x5000 :
            case 0x6000 :
            case 0x7000 :
             if (this.running)
             {
                this.cartridge.addressWrite(addr, data);
             }
             break;

            case 0x8000 :
            case 0x9000 :
             this.graphicsChip.addressWrite(addr - 0x8000, (byte) data);
             break;

            case 0xA000 :
            case 0xB000 :
             this.cartridge.addressWrite(addr, data);
             break;

            case 0xC000 :
             this.mainRam[addr - 0xC000] = (byte) data;
             break;

            case 0xD000 :
             this.mainRam[addr - 0xD000 + (gbcRamBank * 0x1000)] = (byte) data;
             break;

            case 0xE000 :
             this.mainRam[addr - 0xE000] = (byte) data;
             break;

            case 0xF000 :
             if (addr < 0xFE00)
             {
                try
                {
                    this.mainRam[addr - 0xE000] = (byte) data;
                } catch (ArrayIndexOutOfBoundsException e)
                {
                    System.out.println("Address error: " + addr + " pc = " + JayBoy.hexWord(this.pc));
                }
             }
             else if (addr < 0xFF00)
             {
                this.oam[addr - 0xFE00] = (byte) data;
             }
             else
             {
                this.ioHandler.ioWrite(addr - 0xFF00, (short) data);
             }
             break;
        }
    }

    /**BC setter */
    public void setBC(int value)
    {
        this.b = (short) ((value & 0xFF00) >> 8);
        this.c = (short) (value & 0x00FF);
    }

    /**DE setter */
    public void setDE(int value)
    {
        this.d = (short) ((value & 0xFF00) >> 8);
        this.e = (short) (value & 0x00FF);
    }

    /**HL setter */
    public void setHL(int value)
    {
        this.hl = value;
    }

    /** Performs a read of a register by internal register number */
    public final int registerRead(int regNum)
    {
        switch (regNum)
        {
            case 0 : return this.b;
            case 1 : return this.c;
            case 2 : return this.d;
            case 3 : return this.e;
            case 4 : return (short) ((this.hl & 0xFF00) >> 8);
            case 5 : return (short) (this.hl & 0x00FF);
            case 6 : return JayBoy.unsign(addressRead(this.hl));
            case 7 : return this.a;
            default: return -1;
        }
    }

    /** Performs a write of a register by internal register number */
    public final void registerWrite(int regNum, int data)
    {
        switch (regNum)
        {
            case 0 : this.b = (short) data;
                     return;
            case 1 : this.c = (short) data;
                     return;
            case 2 : this.d = (short) data;
                     return;
            case 3 : this.e = (short) data;
                     return;
            case 4 : this.hl = (this.hl & 0x00FF) | (data << 8);
                     return;
            case 5 : this.hl = (this.hl & 0xFF00) | data;
                     return;
            case 6 : addressWrite(this.hl, data);
                     return;
            case 7 : this.a = (short) data;
                     return;
            default: return;
        }
    }


    /** Resets the CPU to it's power on state.  Memory contents are not cleared. */
    public void reset()
    {
        setDoubleSpeedCpu(false);
        this.graphicsChip.dispose(); //clearing memory of gfx chip
        this.cartridge.reset(); //resetting
        this.interruptsEnabled = false;
        this.ieDelay = -1;
        //resetting everything to it's initial state
        this.pc = 0x0100;
        this.sp = 0xFFFE;
        this.f = 0xB0;
        this.gbcRamBank = 1;
        this.instrCount = 0;

        this.a = 0x01;

        //resetting mainRam
        for (int r = 0; r < 0x8000; r++)
        {
            this.mainRam[r] = 0;
        }

        setBC(0x0013);
        setDE(0x00D8);
        setHL(0x014D);
        JayBoy.debugLog("CPU reset");

        this.ioHandler.reset();
    }

    public void setDoubleSpeedCpu(boolean enabled)
    {
        if (enabled) //multiplying by two
        {
            this.INSTRS_PER_HBLANK = BASE_INSTRS_PER_HBLANK * 2;
            this.INSTRS_PER_DIV = BASE_INSTRS_PER_DIV * 2;
        }
        else
        {
            this.INSTRS_PER_HBLANK = BASE_INSTRS_PER_HBLANK;
            this.INSTRS_PER_DIV = BASE_INSTRS_PER_DIV;
        }
    }

    /** If an interrupt is enabled an the interrupt register shows that it has occured, jump to
     *  the relevant interrupt vector address
     */
    public final void checkInterrupts()
    {
        int intFlags = this.ioHandler.registers[0x0F];
        int ieReg = this.ioHandler.registers[0xFF];
        if ((intFlags & ieReg) != 0)
        {
            this.sp -= 2;
            addressWrite(this.sp + 1, this.pc >> 8);  // Push current program counter onto stack
            addressWrite(this.sp, this.pc & 0x00FF);
            this.interruptsEnabled = false;
        
            if ((intFlags & ieReg & INT_VBLANK) != 0)
            {
                this.pc = 0x40;                      // Jump to Vblank interrupt address
                intFlags -= INT_VBLANK;
            }
            else if ((intFlags & ieReg & INT_LCDC) != 0)
            {
                this.pc = 0x48;                      // Jump to LCDC interrupt address
                intFlags -= INT_LCDC;
            }
            else if ((intFlags & ieReg & INT_TIMA) != 0)
            {
                this.pc = 0x50;                      // Jump to TIMA interrupt address
                intFlags -= INT_TIMA;
            }
            else if ((intFlags & ieReg & INT_SER) != 0)
            {
                this.pc = 0x58;                      // Jump to SER interrupt address
                intFlags -= INT_SER;
            }
            else if ((intFlags & ieReg & INT_P10) != 0)
            {// Joypad interrupt
                this.pc = 0x60;                      // Jump to JoyPad interrupt address
                intFlags -= INT_P10;
            }

            this.ioHandler.registers[0x0F] = (byte) intFlags;
            this.inInterrupt = true;
        }
    }

    /** Initiate an interrupt of the specified type */
    public final void triggerInterrupt(int intr)
    {
       this.ioHandler.registers[0x0F] |= intr;
    }

    public final void triggerInterruptIfEnabled(int intr)
    {
       if ((this.ioHandler.registers[0xFF] & (short) (intr)) != 0)
       {
           this.ioHandler.registers[0x0F] |= intr;
       }
    }

    /** Check for interrupts that need to be initiated */
    public final void initiateInterrupts()
    {
        if (this.timaEnabled && ((this.instrCount % this.instrsPerTima) == 0))
        {
            if (JayBoy.unsign(this.ioHandler.registers[05]) == 0)
            {
                this.ioHandler.registers[05] = this.ioHandler.registers[06]; // Set TIMA module
                if ((this.ioHandler.registers[0xFF] & INT_TIMA) != 0)
                    triggerInterrupt(INT_TIMA);
            }
            this.ioHandler.registers[05]++;
        }
      
        if ((this.instrCount % this.INSTRS_PER_DIV) == 0)
        {
            this.ioHandler.registers[04]++;
        }
      
        if ((this.instrCount % this.INSTRS_PER_HBLANK) == 0)
        {
            // LCY Coincidence
        	// The +1 is due to the LCY register being just about to be incremented
        	int cline = JayBoy.unsign(this.ioHandler.registers[0x44]) + 1;
        	if (cline == 152) cline = 0;
        
            if (((this.ioHandler.registers[0xFF] & INT_LCDC) != 0) &&
        	     ((this.ioHandler.registers[0x41] & 64) != 0) &&
                (JayBoy.unsign(this.ioHandler.registers[0x45]) == cline) && ((this.ioHandler.registers[0x40] & 0x80) != 0) && (cline < 0x90))
            {
                triggerInterrupt(INT_LCDC);
            }
       
        	// Trigger on every line
            if (((this.ioHandler.registers[0xFF] & INT_LCDC) != 0) &&
                ((this.ioHandler.registers[0x41] & 0x8) != 0) && ((this.ioHandler.registers[0x40] & 0x80) != 0) && (cline < 0x90) )
                {
                    triggerInterrupt(INT_LCDC);
                }
         
            if (JayBoy.unsign(this.ioHandler.registers[0x44]) == 143)
            {                       //VBlank
               for (int r = 144; r < 170; r++)
               {
                   this.graphicsChip.notifyScanline(r);
               } 
               if ( ((this.ioHandler.registers[0x40] & 0x80) != 0) && ((this.ioHandler.registers[0xFF] & INT_VBLANK) != 0))
               {
                   triggerInterrupt(INT_VBLANK);
                   if ( ((this.ioHandler.registers[0x41] & 16) != 0) && ((this.ioHandler.registers[0xFF] & INT_LCDC) != 0) )
                   {
                       triggerInterrupt(INT_LCDC); //VBLANK LCDC
            	    }
               }
        
                boolean speedThrottle = true;
                if ((speedThrottle) && (this.graphicsChip.frameWaitTime >= 0))
                {
                      try
                      {
                          java.lang.Thread.sleep(this.graphicsChip.frameWaitTime);
                      }
                      catch (InterruptedException e) {
                          // Nothing.
                      }
                }
            }


	    this.graphicsChip.notifyScanline(JayBoy.unsign(this.ioHandler.registers[0x44]));
        this.ioHandler.registers[0x44] = (byte) (JayBoy.unsign(this.ioHandler.registers[0x44]) + 1);

        if (JayBoy.unsign(this.ioHandler.registers[0x44]) >= 153)
            {//VBLANK
                this.ioHandler.registers[0x44] = 0;
                this.graphicsChip.frameDone = false;
                if (JayBoy.getRunningAsApplet())
                {
                    ((JayBoy) (this.applet)).drawNextFrame();
                }
                else
                {
                    ((GameBoyScreen) (this.applet)).repaint();
	            }
                try
                {
                    while (!this.graphicsChip.frameDone)
                    {
                        java.lang.Thread.sleep(1);
                    }
                } catch (InterruptedException e) {
                    // Nothing.
                }
            }
        }
    }

    /**
     *          OPCODES:
     *                  http://www.zilog.com/docs/z80/um0080.pdf
     *                  https://clrhome.org/table/
     *                  https://www.pastraiser.com/cpu/gameboy/gameboy_opcodes.html
     * & (bitwise and) - Binary AND Operator copies a bit to the result if it exists in both operands.
     * | (bitwise or) - Binary OR Operator copies a bit if it exists in either operand.
     * ^ (bitwise XOR) - Binary XOR Operator copies the bit if it is set in one operand but not both.
     * ~ (bitwise compliment) - Binary Ones Complement Operator is unary and has the effect of 'flipping' bits.
     * << (left shift) - Binary Left Shift Operator. The left operands value is moved left by the number of bits
     *                   specified by the right operand.
     * >> (right shift) - Binary Right Shift Operator. The left operands value is moved right by the number of
     *                    bits specified by the right operand.
     * >>> (zero fill right shift) - Shift right zero fill operator. The left operands value is moved right by the
     *                               number of bits specified by the right operand and shifted values are filled up with zeros.
     */

    /** Execute the specified number of Gameboy instructions.  Use '-1' to execute forever */
    public final void execute(int numInstr)
    {
        this.terminate = false;
        this.running = true;
        short newf;
        int dat;
        this.graphicsChip.startTime = System.currentTimeMillis();
        int b1, b2, b3, offset; //holders for Program Counter
    
        for (int r = 0; (r != numInstr) && (!this.terminate); r++)
        {   
            this.instrCount++; //updating instrCount
            
            b1 = JayBoy.unsign(addressRead(this.pc)); //current pc
            offset = addressRead(this.pc + 1); //next pc
            b2 = JayBoy.unsign((short) offset);
            b3 = JayBoy.unsign(addressRead(this.pc + 2)); //current pc+2
            
            switch (b1)
            {   
                case 0x00 :               // NOP
                    this.pc++;
                    break;
                case 0x01 :               // LD BC, nn
                    this.pc+=3;
                    this.b = b3;
                    this.c = b2;
                    break;
                case 0x02 :               // LD (BC), A
                    this.pc++;
                    addressWrite((this.b << 8) | this.c, this.a);
                    break;
                case 0x03 :               // INC BC
                    this.pc++;
                    this.c++;
                    if (this.c == 0x0100) {
                     this.b++;
                     this.c = 0;
                     if (this.b == 0x0100) {
                      this.b = 0;
                     }
                    }
                    break;
                case 0x04 :               // INC B
                    this.pc++;
                    this.f &= F_CARRY;
                    switch (this.b) {
                     case 0xFF: this.f |= F_HALFCARRY + F_ZERO;
                                this.b = 0x00;
                                break;
                     case 0x0F: this.f |= F_HALFCARRY;
                                this.b = 0x10;
                                break;
                     default:   this.b++;
                                break;
                    }
                    break;
                case 0x05 :               // DEC B
                    this.pc++;
                    this.f &= F_CARRY;
                    this.f |= F_SUBTRACT;
                    switch (this.b) {
                     case 0x00: this.f |= F_HALFCARRY;
                                this.b = 0xFF;
                                break;
                     case 0x10: this.f |= F_HALFCARRY;
                                this.b = 0x0F;
                                break;
                     case 0x01: this.f |= F_ZERO;
                                this.b = 0x00;
                                break;
                     default:   this.b--;
                                break;
                    }
                    break;
                case 0x06 :               // LD B, nn
                    this.pc += 2;
                    this.b = b2;
                    break;
                case 0x07 :               // RLC A
                    this.pc++;
                    this.f = 0;
                 
                    this.a <<= 1;
                 
                    if ((this.a & 0x0100) != 0) {
                     this.f |= F_CARRY;
                     this.a |= 1;
                     this.a &= 0xFF;
                    }
                    if (this.a == 0) {
                     this.f |= F_ZERO;
                    }
                    break;
                case 0x08 :               // LD (nnnn), SP   /* **** May be wrong! **** */
                    this.pc+=3;
                    addressWrite((b3 << 8) + b2 + 1, (this.sp & 0xFF00) >> 8);
                    addressWrite((b3 << 8) + b2, (this.sp & 0x00FF));
                    break;
                case 0x09 :               // ADD HL, BC
                    this.pc++;
                    this.hl = (this.hl + ((this.b << 8) + this.c));
                    if ((this.hl & 0xFFFF0000) != 0) {
                     this.f = (short) ((this.f & (F_SUBTRACT + F_ZERO + F_HALFCARRY)) | (F_CARRY));
                     this.hl &= 0xFFFF;
                    } else {
                     this.f = (short) ((this.f & (F_SUBTRACT + F_ZERO + F_HALFCARRY)));
                    }
                    break;
                case 0x0A :               // LD A, (BC)
                    this.pc++;
                    this.a = JayBoy.unsign(addressRead((this.b << 8) + this.c));
                    break;
                case 0x0B :               // DEC BC
                    this.pc++;
                    this.c--;
                    if ((this.c & 0xFF00) != 0) {
                     this.c = 0xFF;
                     this.b--;
                     if ((this.b & 0xFF00) != 0) {
                      this.b = 0xFF;
                     }
                    }
                    break;
                case 0x0C :               // INC C
                    this.pc++;
                    this.f &= F_CARRY;
                    switch (this.c) {
                     case 0xFF: this.f |= F_HALFCARRY + F_ZERO;
                                this.c = 0x00;
                                break;
                     case 0x0F: this.f |= F_HALFCARRY;
                                this.c = 0x10;
                                break;
                     default:   this.c++;
                                break;
                    }
                    break;
                case 0x0D :               // DEC C
                    this.pc++;
                    this.f &= F_CARRY;
                    this.f |= F_SUBTRACT;
                    switch (this.c) {
                     case 0x00: this.f |= F_HALFCARRY;
                                this.c = 0xFF;
                                break;
                     case 0x10: this.f |= F_HALFCARRY;
                                this.c = 0x0F;
                                break;
                     case 0x01: this.f |= F_ZERO;
                                this.c = 0x00;
                                break;
                     default:   this.c--;
                                break;
                    }
                    break;
                case 0x0E :               // LD C, nn
                    this.pc+=2;
                    this.c = b2;
                    break;        
                case 0x0F :               // RRC A
                    this.pc++;
                    if ((this.a & 0x01) == 0x01) {
                     this.f = F_CARRY;
                    } else {
                     this.f = 0;
                    }
                    this.a >>= 1;
                    if ((this.f & F_CARRY) == F_CARRY) {
                        this.a |= 0x80;
                    }
                    if (this.a == 0) {
                     this.f |= F_ZERO;
                    }
                    break;
                case 0x10 :               // STOP
                    this.pc+=2;
                    break;
                case 0x11 :               // LD DE, nnnn
                    this.pc+=3;
                    this.d = b3;
                    this.e = b2;
                    break;
                case 0x12 :               // LD (DE), A
                    this.pc++;
                    addressWrite((this.d << 8) + this.e, this.a);
                    break;
                case 0x13 :               // INC DE
                    this.pc++;
                    this.e++;
                    if (this.e == 0x0100) {
                     this.d++;
                     this.e = 0;
                     if (this.d == 0x0100) {
                      this.d = 0;
                     }
                    }
                    break;
                case 0x14 :               // INC D
                    this.pc++;
                    this.f &= F_CARRY;
                    switch (this.d) {
                     case 0xFF: this.f |= F_HALFCARRY + F_ZERO;
                                this.d = 0x00;
                                break;
                     case 0x0F: this.f |= F_HALFCARRY;
                                this.d = 0x10;
                                break;
                     default:   this.d++;
                                break;
                    }
                    break;
                case 0x15 :               // DEC D
                    this.pc++;
                    this.f &= F_CARRY;
                    this.f |= F_SUBTRACT;
                    switch (this.d) {
                     case 0x00: this.f |= F_HALFCARRY;
                                this.d = 0xFF;
                                break;
                     case 0x10: this.f |= F_HALFCARRY;
                                this.d = 0x0F;
                                break;
                     case 0x01: this.f |= F_ZERO;
                                this.d = 0x00;
                                break;
                     default:   this.d--;
                                break;
                    }
                    break;
                case 0x16 :               // LD D, nn
                    this.pc += 2;
                    this.d = b2;
                    break;
                case 0x17 :               // RL A
                    this.pc++;
                    if ((this.a & 0x80) == 0x80) {
                     newf = F_CARRY;
                    } else {
                     newf = 0;
                    }
                    this.a <<= 1;
                 
                    if ((this.f & F_CARRY) == F_CARRY) {
                        this.a |= 1;
                    }
                 
                    this.a &= 0xFF;
                    if (this.a == 0) {
                     newf |= F_ZERO;
                    }
                    this.f = newf;
                    break;
                case 0x18 :               // JR nn
                    this.pc += 2 + offset;
                    break;
                case 0x19 :               // ADD HL, DE
                    this.pc++;
                    this.hl = (this.hl + ((this.d << 8) + this.e));
                    if ((this.hl & 0xFFFF0000) != 0) {
                     this.f = (short) ((this.f & (F_SUBTRACT + F_ZERO + F_HALFCARRY)) | (F_CARRY));
                     this.hl &= 0xFFFF;
                    } else {
                     this.f = (short) ((this.f & (F_SUBTRACT + F_ZERO + F_HALFCARRY)));
                    }
                    break;
                case 0x1A :               // LD A, (DE)
                    this.pc++;
                    this.a = JayBoy.unsign(addressRead((this.d << 8) + this.e));
                    break;
                case 0x1B :               // DEC DE
                    this.pc++;
                    this.e--;
                    if ((this.e & 0xFF00) != 0) {
                     this.e = 0xFF;
                     this.d--;
                     if ((this.d & 0xFF00) != 0) {
                      this.d = 0xFF;
                     }
                    }
                    break;
                case 0x1C :               // INC E
                    this.pc++;
                    this.f &= F_CARRY;
                    switch (this.e) {
                     case 0xFF: this.f |= F_HALFCARRY + F_ZERO;
                                this.e = 0x00;
                                break;
                     case 0x0F: this.f |= F_HALFCARRY;
                                this.e = 0x10;
                                break;
                     default:   this.e++;
                                break;
                    }
                    break;
                case 0x1D :               // DEC E
                    this.pc++;
                    this.f &= F_CARRY;
                    this.f |= F_SUBTRACT;
                    switch (this.e) {
                     case 0x00: this.f |= F_HALFCARRY;
                                this.e = 0xFF;
                                break;
                     case 0x10: this.f |= F_HALFCARRY;
                                this.e = 0x0F;
                                break;
                     case 0x01: this.f |= F_ZERO;
                                this.e = 0x00;
                                break;
                     default:   this.e--;
                                break;
                    }
                    break;
                case 0x1E :               // LD E, nn
                    this.pc+=2;
                    this.e = b2;
                    break;
                case 0x1F :               // RR A
                    this.pc++;
                    if ((this.a & 0x01) == 0x01) {
                     newf = F_CARRY;
                    } else {
                     newf = 0;
                    }
                    this.a >>= 1;
                 
                    if ((this.f & F_CARRY) == F_CARRY) {
                     this.a |= 0x80;
                    }
                 
                    if (this.a == 0) {
                     newf |= F_ZERO;
                    }
                    this.f = newf;
                    break;
                case 0x20 :               // JR NZ, nn
                    if ((this.f & 0x80) == 0x00) {
                     this.pc += 2 + offset;
                    } else {
                     this.pc += 2;
                    }
                    break;
                case 0x21 :               // LD HL, nnnn
                    this.pc += 3;
                    this.hl = (b3 << 8) + b2;
                    break;
                case 0x22 :               // LD (HL+), A
                    this.pc++;
                    addressWrite(this.hl, this.a);
                    this.hl = (this.hl + 1) & 0xFFFF;
                    break;
                case 0x23 :               // INC HL
                    this.pc++;
                    this.hl = (this.hl + 1) & 0xFFFF;
                    break;
                case 0x24 :               // INC H         ** May be wrong **
                    this.pc++;
                    this.f &= F_CARRY;
                    switch ((this.hl & 0xFF00) >> 8) {
                     case 0xFF: this.f |= F_HALFCARRY + F_ZERO;
                                this.hl = (this.hl & 0x00FF);
                                break;
                     case 0x0F: this.f |= F_HALFCARRY;
                                this.hl = (this.hl & 0x00FF) | 0x10;
                                break;
                     default:   this.hl = (this.hl + 0x0100);
                                break;
                    }
                    break;
                case 0x25 :               // DEC H           ** May be wrong **
                    this.pc++;
                    this.f &= F_CARRY;
                    this.f |= F_SUBTRACT;
                    switch ((this.hl & 0xFF00) >> 8) {
                     case 0x00: this.f |= F_HALFCARRY;
                                this.hl = (this.hl & 0x00FF) | (0xFF00);
                                break;
                     case 0x10: this.f |= F_HALFCARRY;
                                this.hl = (this.hl & 0x00FF) | (0x0F00);
                                break;
                     case 0x01: this.f |= F_ZERO;
                                this.hl = (this.hl & 0x00FF);
                                break;
                     default:   this.hl = (this.hl & 0x00FF) | ((this.hl & 0xFF00) - 0x0100);
                                break;
                    }
                    break;
                case 0x26 :               // LD H, nn
                    this.pc+=2;
                    this.hl = (this.hl & 0x00FF) | (b2 << 8);
                    break;
                case 0x27 :               // DAA         ** This could be wrong! **
                    this.pc++;
                 
                    int upperNibble = (this.a & 0xF0) >> 4;
                    int lowerNibble = this.a & 0x0F;
                 
                 
                    newf = (short) (this.f & F_SUBTRACT);
                 
                    if ((this.f & F_SUBTRACT) == 0) {
                    
                     if ((this.f & F_CARRY) == 0) {
                      if ((upperNibble <= 8) && (lowerNibble >= 0xA) &&
                         ((this.f & F_HALFCARRY) == 0)) {
                       this.a += 0x06;
                      }
                   
                      if ((upperNibble <= 9) && (lowerNibble <= 0x3) &&
                         ((this.f & F_HALFCARRY) == F_HALFCARRY)) {
                       this.a += 0x06;
                      }
                   
                      if ((upperNibble >= 0xA) && (lowerNibble <= 0x9) &&
                         ((this.f & F_HALFCARRY) == 0)) {
                       this.a += 0x60;
                       newf |= F_CARRY;
                      }
                   
                      if ((upperNibble >= 0x9) && (lowerNibble >= 0xA) &&
                         ((this.f & F_HALFCARRY) == 0)) {
                       this.a += 0x66;
                       newf |= F_CARRY;
                      }
                   
                      if ((upperNibble >= 0xA) && (lowerNibble <= 0x3) &&
                         ((this.f & F_HALFCARRY) == F_HALFCARRY)) {
                       this.a += 0x66;
                       newf |= F_CARRY;
                      }
                   
                     } else {  // If carry set
                    
                      if ((upperNibble <= 0x2) && (lowerNibble <= 0x9) &&
                         ((this.f & F_HALFCARRY) == 0)) {
                       this.a += 0x60;
                       newf |= F_CARRY;
                      }
                   
                      if ((upperNibble <= 0x2) && (lowerNibble >= 0xA) &&
                         ((this.f & F_HALFCARRY) == 0)) {
                       this.a += 0x66;
                       newf |= F_CARRY;
                      }
                   
                      if ((upperNibble <= 0x3) && (lowerNibble <= 0x3) &&
                         ((this.f & F_HALFCARRY) == F_HALFCARRY)) {
                       this.a += 0x66;
                       newf |= F_CARRY;
                      }
                   
                     }
                  
                    } else { // Subtract is set
                    
                     if ((this.f & F_CARRY) == 0) {
                    
                      if ((upperNibble <= 0x8) && (lowerNibble >= 0x6) &&
                         ((this.f & F_HALFCARRY) == F_HALFCARRY)) {
                       this.a += 0xFA;
                      }
                   
                     } else { // Carry is set
                    
                      if ((upperNibble >= 0x7) && (lowerNibble <= 0x9) &&
                         ((this.f & F_HALFCARRY) == 0)) {
                       this.a += 0xA0;
                       newf |= F_CARRY;
                      }
                   
                      if ((upperNibble >= 0x6) && (lowerNibble >= 0x6) &&
                         ((this.f & F_HALFCARRY) == F_HALFCARRY)) {
                       this.a += 0x9A;
                       newf |= F_CARRY;
                      }
                   
                     }
                  
                    }
                 
                    this.a &= 0x00FF;
                    if (this.a == 0) newf |= F_ZERO;
                 
                    this.f = newf;
                 
                    break;
                case 0x28 :               // JR Z, nn
                    if ((this.f & F_ZERO) == F_ZERO) {
                     this.pc += 2 + offset;
                    } else {
                     this.pc += 2;
                    }
                    break;
                case 0x29 :               // ADD HL, HL
                    this.pc++;
                    this.hl = (this.hl + this.hl);
                    if ((this.hl & 0xFFFF0000) != 0) {
                     this.f = (short) ((this.f & (F_SUBTRACT + F_ZERO + F_HALFCARRY)) | (F_CARRY));
                     this.hl &= 0xFFFF;
                    } else {
                     this.f = (short) ((this.f & (F_SUBTRACT + F_ZERO + F_HALFCARRY)));
                    }
                    break;
                case 0x2A :               // LDI A, (HL)
                    this.pc++;                    
                    this.a = JayBoy.unsign(addressRead(this.hl));
                    this.hl++;
                    break;
                case 0x2B :               // DEC HL
                    this.pc++;
                    if (this.hl == 0) {
                     this.hl = 0xFFFF;
                    } else {
                     this.hl--;
                    }
                    break;
                case 0x2C :               // INC L
                    this.pc++;
                    this.f &= F_CARRY;
                    switch (this.hl & 0x00FF) {
                     case 0xFF: this.f |= F_HALFCARRY + F_ZERO;
                                this.hl = this.hl & 0xFF00;
                                break;
                     case 0x0F: this.f |= F_HALFCARRY;
                                this.hl++;
                                break;
                     default:   this.hl++;
                                break;
                    }
                    break;
                case 0x2D :               // DEC L
                    this.pc++;
                    this.f &= F_CARRY;
                    this.f |= F_SUBTRACT;
                    switch (this.hl & 0x00FF) {
                     case 0x00: this.f |= F_HALFCARRY;
                                this.hl = (this.hl & 0xFF00) | 0x00FF;
                                break;
                     case 0x10: this.f |= F_HALFCARRY;
                                this.hl = (this.hl & 0xFF00) | 0x000F;
                                break;
                     case 0x01: this.f |= F_ZERO;
                                this.hl = (this.hl & 0xFF00);
                                break;
                     default:   this.hl = (this.hl & 0xFF00) | ((this.hl & 0x00FF) - 1);
                                break;
                    }
                    break;
                case 0x2E :               // LD L, nn
                    this.pc+=2;
                    this.hl = (this.hl & 0xFF00) | b2;
                    break;
                case 0x2F :               // CPL A
                    this.pc++;
                    short mask = 0x80;
                    this.a = (short) ((~this.a) & 0x00FF);
                    this.f = (short) ((this.f & (F_CARRY | F_ZERO)) | F_SUBTRACT | F_HALFCARRY);
                    break;
                case 0x30 :               // JR NC, nn
                    if ((this.f & F_CARRY) == 0) {
                     this.pc += 2 + offset;
                    } else {
                     this.pc += 2;
                    }
                    break;
                case 0x31 :               // LD SP, nnnn
                    this.pc += 3;
                    this.sp = (b3 << 8) + b2;
                    break;
                case 0x32 :
                    this.pc++;
                    addressWrite(this.hl, this.a);  // LD (HL-), A
                    this.hl--;
                    break;
                case 0x33 :               // INC SP
                    this.pc++;
                    this.sp = (this.sp + 1) & 0xFFFF;
                    break;
                case 0x34 :               // INC (HL)
                    this.pc++;
                    this.f &= F_CARRY;
                    dat = JayBoy.unsign(addressRead(this.hl));
                    switch (dat) {
                     case 0xFF: this.f |= F_HALFCARRY + F_ZERO;
                                addressWrite(this.hl, 0x00);
                                break;
                     case 0x0F: this.f |= F_HALFCARRY;
                                addressWrite(this.hl, 0x10);
                                break;
                     default:   addressWrite(this.hl, dat + 1);
                                break;
                    }
                    break;
                case 0x35 :               // DEC (HL)
                    this.pc++;
                    this.f &= F_CARRY;
                    this.f |= F_SUBTRACT;
                    dat = JayBoy.unsign(addressRead(this.hl));
                    switch (dat) {
                     case 0x00: this.f |= F_HALFCARRY;
                                addressWrite(this.hl, 0xFF);
                                break;
                     case 0x10: this.f |= F_HALFCARRY;
                                addressWrite(this.hl, 0x0F);
                                break;
                     case 0x01: this.f |= F_ZERO;
                                addressWrite(this.hl, 0x00);
                                break;
                     default:   addressWrite(this.hl, dat - 1);
                                break;
                    }
                    break;
                case 0x36 :               // LD (HL), nn
                    this.pc += 2;
                    addressWrite(this.hl, b2);
                    break;
                case 0x37 :               // SCF
                    this.pc++;
                    this.f &= F_ZERO;
                    this.f |= F_CARRY;
                    break;
                case 0x38 :               // JR C, nn
                    if ((this.f & F_CARRY) == F_CARRY) {
                     this.pc += 2 + offset;
                    } else {
                     this.pc += 2;
                    }
                    break;
                case 0x39 :               // ADD HL, SP      ** Could be wrong **
                    this.pc++;
                    this.hl = (this.hl + this.sp);
                    if ((this.hl & 0xFFFF0000) != 0) {
                     this.f = (short) ((this.f & (F_SUBTRACT + F_ZERO + F_HALFCARRY)) | (F_CARRY));
                     this.hl &= 0xFFFF;
                    } else {
                     this.f = (short) ((this.f & (F_SUBTRACT + F_ZERO + F_HALFCARRY)));
                    }
                    break;
                case 0x3A :               // LD A, (HL-)
                    this.pc++;
                    this.a = JayBoy.unsign(addressRead(this.hl));
                    this.hl = (this.hl - 1) & 0xFFFF;
                    break;
                case 0x3B :               // DEC SP
                    this.pc++;
                    this.sp = (this.sp - 1) & 0xFFFF;
                    break;
                case 0x3C :               // INC A
                    this.pc++;
                    this.f &= F_CARRY;
                    switch (this.a) {
                     case 0xFF: this.f |= F_HALFCARRY + F_ZERO;
                                this.a = 0x00;
                                break;
                     case 0x0F: this.f |= F_HALFCARRY;
                                this.a = 0x10;
                                break;
                     default:   this.a++;
                                break;
                    }
                    break;
                case 0x3D :               // DEC A
                    this.pc++;
                    this.f &= F_CARRY;
                    this.f |= F_SUBTRACT;
                    switch (this.a) {
                     case 0x00: this.f |= F_HALFCARRY;
                                this.a = 0xFF;
                                break;
                     case 0x10: this.f |= F_HALFCARRY;
                                this.a = 0x0F;
                                break;
                     case 0x01: this.f |= F_ZERO;
                                this.a = 0x00;
                                break;
                     default:   this.a--;
                                break;
                    }
                    break;
                case 0x3E :               // LD A, nn
                    this.pc += 2;
                    this.a = b2;
                    break;
                case 0x3F :               // CCF
                    this.pc++;
                    if ((this.f & F_CARRY) == 0) {
                     this.f = (short) ((this.f & F_ZERO) | F_CARRY);
                    } else {
                     this.f = (short) (this.f & F_ZERO);
                    }
                    break;
                case 0x52 :               // Debug breakpoint (LD D, D)
                	    // As this insturction is used in games (why?) only break here if the breakpoint is on in the debugger
                		if (this.breakpointEnable) {
                     this.terminate = true;
                     System.out.println("- Breakpoint reached");
                		} else {
                		 this.pc++;
                		}
                    break;
                    
                case 0x76 :               // HALT
                	    this.interruptsEnabled = true;
                    while (this.ioHandler.registers[0x0F] == 0) {
                     initiateInterrupts();
                     this.instrCount++;
                    }
                 
                    this.pc++;
                    break;
                case 0xAF :               // XOR A, A (== LD A, 0)
                    this.pc ++;
                    this.a = 0;
                    this.f = 0x80;             // Set zero flag
                    break;
                case 0xC0 :               // RET NZ
                    if ((this.f & F_ZERO) == 0) {
                     this.pc = (JayBoy.unsign(addressRead(this.sp + 1)) << 8) + JayBoy.unsign(addressRead(this.sp));
                     this.sp += 2;
                    } else {
                     this.pc++;
                    }
                    break;
                case 0xC1 :               // POP BC
                    this.pc++;
                    this.c = JayBoy.unsign(addressRead(this.sp));
                    this.b = JayBoy.unsign(addressRead(this.sp + 1));
                    this.sp+=2;
                    break;
                case 0xC2 :               // JP NZ, nnnn
                    if ((this.f & F_ZERO) == 0) {
                     this.pc = (b3 << 8) + b2;
                    } else {
                     this.pc += 3;
                    }
                    break;
                case 0xC3 :
                    this.pc = (b3 << 8) + b2;  // JP nnnn
                    break;
                case 0xC4 :               // CALL NZ, nnnnn
                    if ((this.f & F_ZERO) == 0) {
                     this.pc += 3;
                     this.sp -= 2;
                     addressWrite(this.sp + 1, this.pc >> 8);
                     addressWrite(this.sp, this.pc & 0x00FF);
                     this.pc = (b3 << 8) + b2;
                    } else {
                     this.pc+=3;
                    }
                    break;
                case 0xC5 :               // PUSH BC
                    this.pc++;
                    this.sp -= 2;
                    this.sp &= 0xFFFF;
                    addressWrite(this.sp, this.c);
                    addressWrite(this.sp + 1, this.b);
                    break;
                case 0xC6 :               // ADD A, nn
                    this.pc+=2;
                    this.f = 0;
                 
                    if ((((this.a & 0x0F) + (b2 & 0x0F)) & 0xF0) != 0x00) {
                     this.f |= F_HALFCARRY;
                    }
                 
                    this.a += b2;
                 
                    if ((this.a & 0xFF00) != 0) {     // Perform 8-bit overflow and set zero flag
                     if (this.a == 0x0100) {
                      this.f |= F_ZERO + F_CARRY + F_HALFCARRY;
                      this.a = 0;
                     } else {
                      this.f |= F_CARRY + F_HALFCARRY;
                      this.a &= 0x00FF;
                     }
                    }
                    break;
                case 0xCF :               // RST 08
                    this.pc++;
                    this.sp -= 2;
                    addressWrite(this.sp + 1, this.pc >> 8);
                    addressWrite(this.sp, this.pc & 0x00FF);
                    this.pc = 0x08;
                    break;
                case 0xC8 :               // RET Z
                    if ((this.f & F_ZERO) == F_ZERO) {
                     this.pc = (JayBoy.unsign(addressRead(this.sp + 1)) << 8) + JayBoy.unsign(addressRead(this.sp));
                     this.sp += 2;
                    } else {
                     this.pc++;
                    }
                    break;
                case 0xC9 :               // RET
                    this.pc = (JayBoy.unsign(addressRead(this.sp + 1)) << 8) + JayBoy.unsign(addressRead(this.sp));
                    this.sp += 2;
                    break;
                case 0xCA :               // JP Z, nnnn
                    if ((this.f & F_ZERO) == F_ZERO) {
                     this.pc = (b3 << 8) + b2;
                    } else {
                     this.pc += 3;
                    }
                    break;
                case 0xCB :               // Shift/bit test
                    this.pc += 2;
                    int regNum = b2 & 0x07;
                    int data = registerRead(regNum);
                    if ((b2 & 0xC0) == 0) {
                     switch ((b2 & 0xF8)) {
                      case 0x00 :          // RLC A
                       if ((data & 0x80) == 0x80) {
                        this.f = F_CARRY;
                       } else {
                        this.f = 0;
                       }
                       data <<= 1;
                       if ((this.f & F_CARRY) == F_CARRY) {
                        data |= 1;
                       }
                    
                       data &= 0xFF;
                       if (data == 0) {
                        this.f |= F_ZERO;
                       }
                       registerWrite(regNum, data);
                       break;
                      case 0x08 :          // RRC A
                       if ((data & 0x01) == 0x01) {
                        this.f = F_CARRY;
                       } else {
                        this.f = 0;
                       }
                       data >>= 1;
                       if ((this.f & F_CARRY) == F_CARRY) {
                        data |= 0x80;
                       }
                       if (data == 0) {
                        this.f |= F_ZERO;
                       }
                       registerWrite(regNum, data);
                       break;
                      case 0x10 :          // RL r
                    
                       if ((data & 0x80) == 0x80) {
                        newf = F_CARRY;
                       } else {
                        newf = 0;
                       }
                       data <<= 1;
                    
                       if ((this.f & F_CARRY) == F_CARRY) {
                        data |= 1;
                       }
                    
                       data &= 0xFF;
                       if (data == 0) {
                        newf |= F_ZERO;
                       }
                       this.f = newf;
                       registerWrite(regNum, data);
                       break;
                      case 0x18 :          // RR r
                       if ((data & 0x01) == 0x01) {
                        newf = F_CARRY;
                       } else {
                        newf = 0;
                       }
                       data >>= 1;
                    
                       if ((this.f & F_CARRY) == F_CARRY) {
                        data |= 0x80;
                       }
                    
                       if (data == 0) {
                        newf |= F_ZERO;
                       }
                       this.f = newf;
                       registerWrite(regNum, data);
                       break;
                      case 0x20 :          // SLA r
                       if ((data & 0x80) == 0x80) {
                        this.f = F_CARRY;
                       } else {
                        this.f = 0;
                       }
                    
                       data <<= 1;
                    
                       data &= 0xFF;
                       if (data == 0) {
                        this.f |= F_ZERO;
                       }
                       registerWrite(regNum, data);
                       break;
                      case 0x28 :          // SRA r
                       short topBit = 0;
                    
                       topBit = (short) (data & 0x80);
                       if ((data & 0x01) == 0x01) {
                        this.f = F_CARRY;
                       } else {
                        this.f = 0;
                       }
                    
                       data >>= 1;
                       data |= topBit;
                    
                       if (data == 0) {
                        this.f |= F_ZERO;
                       }
                       registerWrite(regNum, data);
                       break;
                      case 0x30 :          // SWAP r
                    
                       data = (short) (((data & 0x0F) << 4) | ((data & 0xF0) >> 4));
                       if (data == 0) {
                        this.f = F_ZERO;
                       } else {
                        this.f = 0;
                       }
                       registerWrite(regNum, data);
                       break;
                      case 0x38 :          // SRL r
                       if ((data & 0x01) == 0x01) {
                        this.f = F_CARRY;
                       } else {
                        this.f = 0;
                       }
                    
                       data >>= 1;
                    
                       if (data == 0) {
                        this.f |= F_ZERO;
                       }
                       registerWrite(regNum, data);
                       break;
                      }
                    } else {
                    
                     int bitNumber = (b2 & 0x38) >> 3;
                    
                     if ((b2 & 0xC0) == 0x40)  {  // BIT n, r
                      mask = (short) (0x01 << bitNumber);
                      if ((data & mask) != 0) {
                       this.f = (short) ((this.f & F_CARRY) | F_HALFCARRY);
                      } else {
                       this.f = (short) ((this.f & F_CARRY) | (F_HALFCARRY + F_ZERO));
                      }
                     }
                     if ((b2 & 0xC0) == 0x80) {  // RES n, r
                      mask = (short) (0xFF - (0x01 << bitNumber));
                      data = (short) (data & mask);
                      registerWrite(regNum, data);
                     }
                     if ((b2 & 0xC0) == 0xC0) {  // SET n, r
                      mask = (short) (0x01 << bitNumber);
                      data = (short) (data | mask);
                      registerWrite(regNum, data);
                     }
                  
                    }
                 
                    break;
                case 0xCC :               // CALL Z, nnnnn
                    if ((this.f & F_ZERO) == F_ZERO) {
                     this.pc += 3;
                     this.sp -= 2;
                     addressWrite(this.sp + 1, this.pc >> 8);
                     addressWrite(this.sp, this.pc & 0x00FF);
                     this.pc = (b3 << 8) + b2;
                    } else {
                     this.pc+=3;
                    }
                    break;
                case 0xCD :               // CALL nnnn
                    this.pc += 3;
                    this.sp -= 2;
                    addressWrite(this.sp + 1, this.pc >> 8);
                    addressWrite(this.sp, this.pc & 0x00FF);
                    this.pc = (b3 << 8) + b2;
                    break;
                case 0xCE :               // ADC A, nn
                    this.pc+=2;
                 
                    if ((this.f & F_CARRY) != 0) {
                     b2++;
                    }
                    this.f = 0;
                 
                    if ((((this.a & 0x0F) + (b2 & 0x0F)) & 0xF0) != 0x00) {
                     this.f |= F_HALFCARRY;
                    }
                 
                    this.a += b2;
                 
                    if ((this.a & 0xFF00) != 0) {     // Perform 8-bit overflow and set zero flag
                     if (this.a == 0x0100) {
                      this.f |= F_ZERO + F_CARRY + F_HALFCARRY;
                      this.a = 0;
                     } else {
                      this.f |= F_CARRY + F_HALFCARRY;
                      this.a &= 0x00FF;
                     }
                    }
                    break;
                case 0xC7 :               // RST 00
                    this.pc++;
                    this.sp -= 2;
                    addressWrite(this.sp + 1, this.pc >> 8);
                    addressWrite(this.sp, this.pc & 0x00FF);
                    this.pc = 0x00;
                    break;
                case 0xD0 :               // RET NC
                    if ((this.f & F_CARRY) == 0) {
                     this.pc = (JayBoy.unsign(addressRead(this.sp + 1)) << 8) + JayBoy.unsign(addressRead(this.sp));
                     this.sp += 2;
                    } else {
                     this.pc++;
                    }
                    break;
                case 0xD1 :               // POP DE
                    this.pc++;
                    this.e = JayBoy.unsign(addressRead(this.sp));
                    this.d = JayBoy.unsign(addressRead(this.sp + 1));
                    this.sp+=2;
                    break;
                case 0xD2 :               // JP NC, nnnn
                    if ((this.f & F_CARRY) == 0) {
                     this.pc = (b3 << 8) + b2;
                    } else {
                     this.pc += 3;
                    }
                    break;
                case 0xD4 :               // CALL NC, nnnn
                    if ((this.f & F_CARRY) == 0) {
                     this.pc += 3;
                     this.sp -= 2;
                     addressWrite(this.sp + 1, this.pc >> 8);
                     addressWrite(this.sp, this.pc & 0x00FF);
                     this.pc = (b3 << 8) + b2;
                    } else {
                     this.pc+=3;
                    }
                    break;
                case 0xD5 :               // PUSH DE
                    this.pc++;
                    this.sp -= 2;
                    this.sp &= 0xFFFF;
                    addressWrite(this.sp, this.e);
                    addressWrite(this.sp + 1, this.d);
                    break;
                case 0xD6 :               // SUB A, nn
                    this.pc+=2;
                 
                    this.f = F_SUBTRACT;
                 
                    if ((((this.a & 0x0F) - (b2 & 0x0F)) & 0xFFF0) != 0x00) {
                     this.f |= F_HALFCARRY;
                    }
                 
                    this.a -= b2;
                 
                    if ((this.a & 0xFF00) != 0) {
                     this.a &= 0x00FF;
                     this.f |= F_CARRY;
                     }
                     if (this.a == 0) {
                      this.f |= F_ZERO;
                     }
                     break;
                case 0xD7 :               // RST 10
                    this.pc++;
                    this.sp -= 2;
                    addressWrite(this.sp + 1, this.pc >> 8);
                    addressWrite(this.sp, this.pc & 0x00FF);
                    this.pc = 0x10;
                    break;
                case 0xD8 :               // RET C
                    if ((this.f & F_CARRY) == F_CARRY) {
                     this.pc = (JayBoy.unsign(addressRead(this.sp + 1)) << 8) + JayBoy.unsign(addressRead(this.sp));
                     this.sp += 2;
                    } else {
                     this.pc++;
                    }
                    break;
                case 0xD9 :               // RETI
                    this.interruptsEnabled = true;
                    this.inInterrupt = false;
                    this.pc = (JayBoy.unsign(addressRead(this.sp + 1)) << 8) + JayBoy.unsign(addressRead(this.sp));
                    this.sp += 2;
                    break;
                case 0xDA :               // JP C, nnnn
                    if ((this.f & F_CARRY) == F_CARRY) {
                     this.pc = (b3 << 8) + b2;
                    } else {
                     this.pc += 3;
                    }
                    break;
                case 0xDC :               // CALL C, nnnn
                    if ((this.f & F_CARRY) == F_CARRY) {
                     this.pc += 3;
                     this.sp -= 2;
                     addressWrite(this.sp + 1, this.pc >> 8);
                     addressWrite(this.sp, this.pc & 0x00FF);
                     this.pc = (b3 << 8) + b2;
                    } else {
                     this.pc+=3;
                    }
                    break;
                case 0xDE :               // SBC A, nn
                    this.pc+=2;
                    if ((this.f & F_CARRY) != 0) {
                     b2++;
                    }
                 
                    this.f = F_SUBTRACT;
                    if ((((this.a & 0x0F) - (b2 & 0x0F)) & 0xFFF0) != 0x00) {
                     this.f |= F_HALFCARRY;
                    }
                 
                    this.a -= b2;
                 
                    if ((this.a & 0xFF00) != 0) {
                     this.a &= 0x00FF;
                     this.f |= F_CARRY;
                    }
                 
                    if (this.a == 0) {
                     this.f |= F_ZERO;
                    }
                    break;
                case 0xDF :               // RST 18
                    this.pc++;
                    this.sp -= 2;
                    addressWrite(this.sp + 1, this.pc >> 8);
                    addressWrite(this.sp, this.pc & 0x00FF);
                    this.pc = 0x18;
                    break;
                case 0xE0 :               // LDH (FFnn), A
                    this.pc += 2;
                    addressWrite(0xFF00 + b2, this.a);
                    break;
                case 0xE1 :               // POP HL
                    this.pc++;
                    this.hl = (JayBoy.unsign(addressRead(this.sp + 1)) << 8) + JayBoy.unsign(addressRead(this.sp));
                    this.sp += 2;
                    break;
                case 0xE2 :               // LDH (FF00 + C), A
                    this.pc++;
                    addressWrite(0xFF00 + this.c, this.a);
                    break;
                case 0xE5 :               // PUSH HL
                    this.pc++;
                    this.sp -= 2;
                    this.sp &= 0xFFFF;
                    addressWrite(this.sp + 1, this.hl >> 8);
                    addressWrite(this.sp, this.hl & 0x00FF);
                    break;
                case 0xE6 :               // AND nn
                    this.pc+=2;
                    this.a &= b2;
                    if (this.a == 0) {
                     this.f = F_ZERO;
                    } else {
                     this.f = 0;
                    }
                    break;
                case 0xE7 :               // RST 20
                    this.pc++;
                    this.sp -= 2;
                    addressWrite(this.sp + 1, this.pc >> 8);
                    addressWrite(this.sp, this.pc & 0x00FF);
                    this.pc = 0x20;
                    break;
                case 0xE8 :               // ADD SP, nn
                    this.pc+=2;
                    this.sp = (this.sp + offset);
                    if ((this.sp & 0xFFFF0000) != 0) {
                     this.f = (short) ((this.f & (F_SUBTRACT + F_ZERO + F_HALFCARRY)) | (F_CARRY));
                     this.sp &= 0xFFFF;
                    } else {
                     this.f = (short) ((this.f & (F_SUBTRACT + F_ZERO + F_HALFCARRY)));
                    }
                    break;
                case 0xE9 :               // JP (HL)
                    this.pc++;
                    this.pc = this.hl;
                    break;
                case 0xEA :               // LD (nnnn), A
                    this.pc += 3;              
                    addressWrite((b3 << 8) + b2, this.a);
                    break;
                case 0xEE :               // XOR A, nn
                    this.pc+=2;
                    this.a ^= b2;
                    if (this.a == 0) {
                     this.f = F_ZERO;
                    } else {
                     this.f = 0;
                    }
                    break;
                case 0xEF :               // RST 28
                    this.pc++;
                    this.sp -= 2;
                    addressWrite(this.sp + 1, this.pc >> 8);
                    addressWrite(this.sp, this.pc & 0x00FF);
                    this.pc = 0x28;
                    break;
                case 0xF0 :               // LDH A, (FFnn)
                    this.pc += 2;
                    this.a = JayBoy.unsign(addressRead(0xFF00 + b2));
                    break;
                case 0xF1 :               // POP AF
                    this.pc++;
                    this.f = JayBoy.unsign(addressRead(this.sp));
                    this.a = JayBoy.unsign(addressRead(this.sp + 1));
                    this.sp+=2;
                    break;
                case 0xF2 :               // LD A, (FF00 + C)
                    this.pc++;
                    this.a = JayBoy.unsign(addressRead(0xFF00 + this.c));
                    break;
                case 0xF3 :               // DI
                    this.pc++;
                    this.interruptsEnabled = false;
                    break;
                case 0xF5 :               // PUSH AF
                    this.pc++;
                    this.sp -= 2;
                    this.sp &= 0xFFFF;
                    addressWrite(this.sp, this.f);
                    addressWrite(this.sp + 1, this.a);
                    break;
                case 0xF6 :               // OR A, nn
                    this.pc+=2;
                    this.a |= b2;
                    if (this.a == 0) {
                     this.f = F_ZERO;
                    } else {
                     this.f = 0;
                    }
                    break;
                case 0xF7 :               // RST 30
                    this.pc++;
                    this.sp -= 2;
                    addressWrite(this.sp + 1, this.pc >> 8);
                    addressWrite(this.sp, this.pc & 0x00FF);
                    this.pc = 0x30;
                    break;
                case 0xF8 :               // LD HL, SP + nn  ** HALFCARRY FLAG NOT SET ***
                    this.pc += 2;
                    this.hl = (this.sp + offset);
                    if ((this.hl & 0x10000) != 0) {
                     this.f = F_CARRY;
                     this.hl &= 0xFFFF;
                    } else {
                     this.f = 0;
                    }
                    break;
                case 0xF9 :               // LD SP, HL
                    this.pc++;
                    this.sp = this.hl;
                    break;
                case 0xFA :               // LD A, (nnnn)
                    this.pc+=3;
                    this.a = JayBoy.unsign(addressRead((b3 << 8) + b2));
                    break;
                case 0xFB :               // EI
                    this.pc++;
                    this.ieDelay = 1;
                    break;
                case 0xFE :               // CP nn     ** FLAGS ARE WRONG! **
                    this.pc += 2;
                    this.f = 0;
                    if (b2 == this.a) {
                     this.f |= F_ZERO;
                    } else {
                     if (this.a < b2) {
                      this.f |= F_CARRY;
                     }
                    }
                    break;
                case 0xFF :               // RST 38
                    this.pc++;
                    this.sp -= 2;
                    addressWrite(this.sp + 1, this.pc >> 8);
                    addressWrite(this.sp, this.pc & 0x00FF);
                    this.pc = 0x38;
                    break;
                 
                default :
                    if ((b1 & 0xC0) == 0x80)
                    {       // Byte 0x10?????? indicates ALU op
                        this.pc++;
                        int operand = registerRead(b1 & 0x07);
                        switch ((b1 & 0x38) >> 3) {
                         case 1 : // ADC A, r
                             if ((this.f & F_CARRY) != 0) {
                              operand++;
                             }
                             // Note!  No break!
                         case 0 : // ADD A, r
                          
                             this.f = 0;
                          
                             if ((((this.a & 0x0F) + (operand & 0x0F)) & 0xF0) != 0x00) {
                              this.f |= F_HALFCARRY;
                             }
                          
                             this.a += operand;
                          
                             if (this.a == 0) {
                              this.f |= F_ZERO;
                             }
                          
                             if ((this.a & 0xFF00) != 0) {     // Perform 8-bit overflow and set zero flag
                              if (this.a == 0x0100) {
                               this.f |= F_ZERO + F_CARRY + F_HALFCARRY;
                               this.a = 0;
                              } else {
                               this.f |= F_CARRY + F_HALFCARRY;
                               this.a &= 0x00FF;
                              }
                             }
                             break;
                         case 3 : // SBC A, r
                             if ((this.f & F_CARRY) != 0) {
                              operand++;
                             }
                             // Note! No break!
                         case 2 : // SUB A, r
                          
                             this.f = F_SUBTRACT;
                          
                             if ((((this.a & 0x0F) - (operand & 0x0F)) & 0xFFF0) != 0x00) {
                              this.f |= F_HALFCARRY;
                             }
                          
                             this.a -= operand;
                          
                             if ((this.a & 0xFF00) != 0) {
                              this.a &= 0x00FF;
                              this.f |= F_CARRY;
                             }
                             if (this.a == 0) {
                              this.f |= F_ZERO;
                             }
                          
                             break;
                         case 4 : // AND A, r
                             this.a &= operand;
                             if (this.a == 0) {
                              this.f = F_ZERO;
                             } else {
                              this.f = 0;
                             }
                             break;
                         case 5 : // XOR A, r
                             this.a ^= operand;
                             if (this.a == 0) {
                              this.f = F_ZERO;
                             } else {
                              this.f = 0;
                             }
                             break;
                         case 6 : // OR A, r
                             this.a |= operand;
                             if (this.a == 0) {
                              this.f = F_ZERO;
                             } else {
                              this.f = 0;
                             }
                             break;
                         case 7 : // CP A, r (compare)
                             this.f = F_SUBTRACT;
                             if (this.a == operand) {
                              this.f |= F_ZERO;
                             }
                             if (this.a < operand) {
                              this.f |= F_CARRY;
                             }
                             if ((this.a & 0x0F) < (operand & 0x0F)) {
                              this.f |= F_HALFCARRY;
                             }
                             break;
                        }
                    }
                    else if ((b1 & 0xC0) == 0x40)
                    {   // Byte 0x01xxxxxxx indicates 8-bit ld
                        this.pc++;
                        registerWrite((b1 & 0x38) >> 3, registerRead(b1 & 0x07));
                    }
                    else
                    {
                        System.out.println("Unrecognized opcode (" + JayBoy.hexByte(b1) + ")");
                        this.terminate = true;
                        this.pc++;
                        break;
                    }
            }
            if (this.ieDelay != -1)
            {
                if (this.ieDelay > 0)
                {
                    this.ieDelay--; //decrease ieDelay
                }
                else
                {
                    this.interruptsEnabled = true; //INTERRUPTS!!!!
                    this.ieDelay = -1;
                }
            }

            //If an interrupt is enabled an the interrupt register shows that it has occured, jump to
            //the relevant interrupt vector address
            if (this.interruptsEnabled)
            {
                checkInterrupts();
            }


            initiateInterrupts(); //initiate interrupts

        }
        this.running = false;
        this.terminate = false;
    }
    
}
