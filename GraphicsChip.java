/**
 * This class is the master class for implementations 
 * of the graphics class.
 * A graphics implementation will subclass from this class.
 * It contains methods for calculating the frame rate.
 */

import java.awt.*;
  
abstract class GraphicsChip
{
    /** Tile uses the background palette */ 
    static final int TILE_BKG = 0;

    /** Tile uses the first sprite palette */ 
    static final int TILE_OBJ1 = 4;

    /** Tile uses the second sprite palette */
    static final int TILE_OBJ2 = 8;

    /** Tile is flipped horizontally */
    static final int TILE_FLIPX = 1; 

    /** Tile is flipped vertically */ 
    static final int TILE_FLIPY = 2;

    /** The current contents of the video memory, mapped in at 0x8000 - 0x9FFF */ 
    byte[] videoRam = new byte[0x8000]; 

    /** The background palette */ 
    GameboyPalette backgroundPalette; 

    /** The first sprite palette */ 
    GameboyPalette obj1Palette; 

    /** The second sprite palette */ 
    GameboyPalette obj2Palette;

    boolean spritesEnabled = true;

    boolean bgEnabled = true; //background enabled?
    boolean winEnabled = true;

    /** The image containing the Gameboy screen */ 
    Image backBuffer; 
    
    /** The current frame skip value */
    int frameSkip = 2;
    
    /** The number of frames that have been drawn so far in the current frame sampling period */
    int framesDrawn = 0;
    
    /** Image magnification */
    int mag = 2; 
    int width = 160 * mag; //160 pixels is the width of the gameboy lcd screen
    int height = 144 * mag; //144 pixels is the height of the gameboy lcd screen
    
    /** Amount of time to wait between frames (ms) */ 
    int frameWaitTime = 0; 
    
    /** The current frame has finished drawing */ 
    boolean frameDone = false; 
    int averageFPS = 0; 
    long startTime = 0; 
    
    /** Selection of one of two addresses for the BG and Window tile data areas */ 
    boolean bgWindowDataSelect = true; 
    
    /** If true, 8x16 sprites are being used.  Otherwise, 8x8. */ 
    boolean doubledSprites = false; 
    
    /** Selection of one of two address for the BG tile map. */
    boolean hiBgTileMapAddress= false; 
    /** referencing the current cpu. */
    Cpu cpu;
    /** referencing current JayBoy as Component. */
    Component applet;
    int tileStart = 0;
    int vidRamStart = 0;

    /**
     * Create a new GraphicsChip connected to the speicfied CPU
     */ 
    public GraphicsChip(Component a, Cpu d)
    {  
        this.cpu = d; //referencing the given cpu

        this.backgroundPalette = new GameboyPalette(0, 1, 2, 3); //creating new palettes
        this.obj1Palette = new GameboyPalette(0, 1, 2, 3);
        this.obj2Palette = new GameboyPalette(0, 1, 2, 3);
    
       this.backBuffer = a.createImage(160 * mag, 144 * mag);
       this.applet = a;
    }

    /** Set the magnification for the screen */
    public void setMagnify(int m)
    {
        this.mag = m;
        this.width = m * 160;
        this.height = m * 144;
        if (backBuffer != null) backBuffer.flush();
        this.backBuffer = applet.createImage(this.width, this.height);
    }
    
    /**
     * clear allocated memory
     */
    public void dispose()
    {
        this.backBuffer.flush();
    }

    /**
     * Calculate the number of frames per second for the current sampling period
     */ 
    public void calculateFPS()
    {  
       if (this.startTime == 0)
       {   
            this.startTime = System.currentTimeMillis(); //saving start time of frame sampling
       }
       if (this.framesDrawn > 30)
       {   
            long delay = System.currentTimeMillis() - startTime; //time from start time
            this.averageFPS = (int) ((this.framesDrawn) / (delay / 1000f)); //calculating fps
            this.startTime = System.currentTimeMillis(); //new start time for next calculation
            int timePerFrame;
        
            if (this.averageFPS != 0)
            {
                timePerFrame = 1000 / this.averageFPS; //calculating time per frame
            }
            else
            {
                timePerFrame = 100;
            }
            this.frameWaitTime = 17 - timePerFrame + this.frameWaitTime;

            this.framesDrawn = 0; //resetting for next calculation
        }
    } 

    /**
     * Return the number of frames per second achieved in the previous sampling period.
     */ 
    public int getFPS()
    {  
        return this.averageFPS;
    }

    public int getWidth()
    {
        return this.width;
    }
      
    public int getHeight()
    {
        return this.height;
    }

    abstract public short addressRead(int addr); 
    abstract public void addressWrite(int addr, byte data);

    abstract public void invalidateAll(int attribs);
    abstract public void invalidateAll();

    abstract public boolean draw(Graphics g, int startX, int startY, Component a);

    abstract public void notifyScanline(int line); 
    
    abstract public boolean isFrameReady();
}
