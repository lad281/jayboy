/** This class represents a palette.  There can be three
 *  palettes, one for the background and window, and two
 *  for sprites. 
 */

class GameboyPalette
{
    /** Data for which colour maps to which RGB value */
    short[] data = new short[4];    
    /** Default RGB colour values */
    int[] colours = {0xFFFFFFFF, 0xFFAAAAAA, 0xFF555555, 0xFF000000};

    /** Create a palette with the specified colour mappings */
    public GameboyPalette(int c1, int c2, int c3, int c4)
    {
      this.data[0] = (short) c1;
      this.data[1] = (short) c2;
      this.data[2] = (short) c3;
      this.data[3] = (short) c4;
    }   

    /** Create a palette from the internal Gameboy format */
    public GameboyPalette(int pal)
    {
      decodePalette(pal);
    }

    /** Set the palette from the internal Gameboy format */
    public void decodePalette(int pal)
    {
      this.data[0] = (short) (pal & 0x03);
      this.data[1] = (short) ((pal & 0x0C) >> 2);
      this.data[2] = (short) ((pal & 0x30) >> 4);
      this.data[3] = (short) ((pal & 0xC0) >> 6);
    }   

    /** Get the RGB colour value for a specific colour entry */
    public int getRgbEntry(int e)
    {
      return this.colours[this.data[e]];
    }   
     
    /** Get the colour number for a specific colour entry */
    public short getEntry(int e)
    {
      return this.data[e];
    }
}
