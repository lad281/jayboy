import java.awt.*;
import java.awt.image.*;



/** This class is one implementation of the GraphicsChip.
 *  It performs the output of the graphics screen, including the background, window, and sprite layers.
 *  It supports some raster effects, but only ones that happen on a tile row boundary.
 */
class TileBasedGraphicsChip extends GraphicsChip
{
   /** Tile cache */
   GameboyTile[] tiles = new GameboyTile[384 * 2];

   // Hacks to allow some raster effects to work.  Or at least not to break as badly.
   boolean savedWindowDataSelect = false;
   boolean spritesEnabledThisFrame = false;

   boolean windowEnableThisLine = false;
   int windowStopLine = 144;


   public TileBasedGraphicsChip(Component a, Cpu d)
   {
      super(a, d);
      for (int r = 0; r < 384 * 2; r++)
      {
         this.tiles[r] = new GameboyTile(a);
      }
   }

   /**
    * Flush the tile cache
    */
   public void dispose()
   {
      for (int r = 0; r < 384 * 2; r++)
      {
         if (this.tiles[r] != null)
         {
            this.tiles[r].dispose();
         }
      }
   }

   /**
   * Reads data from the specified video RAM address
   */
   public short addressRead(int addr)
   {
      return this.videoRam[addr + this.vidRamStart];
   }

   /**
    * Writes data to the specified video RAM address
    */
   public void addressWrite(int addr, byte data)
   {
      if (addr < 0x1800)
      {   // Bkg Tile data area
         this.tiles[(addr >> 4) + this.tileStart].invalidate();
         this.videoRam[addr + this.vidRamStart] = data;
      }
      else
      {
         this.videoRam[addr + this.vidRamStart] = data;
      }
   }

   /**
    * Invalidates all tiles in the tile cache that have the given attributes.
    * These will be regenerated next time they are drawn.
    */
   public void invalidateAll(int attribs)
   {
      for (int r = 0; r < 384 * 2; r++)
      {
         this.tiles[r].invalidate(attribs);
      }
   }

   /**
    * Invalidate all tiles in the tile cache
    */
   public void invalidateAll()
   {
      for (int r = 0; r < 384 * 2; r++)
      {
         this.tiles[r].invalidate();
      }
   }

   /**
    * Draw sprites into the back buffer which have the given priority
    */
   public void drawSprites(Graphics back, int priority)
   {
      int vidRamAddress = 0;

      // Draw sprites
      for (int i = 0; i < 40; i++)
      {
         int spriteX = this.cpu.addressRead(0xFE01 + (i * 4)) - 8;
         int spriteY = this.cpu.addressRead(0xFE00 + (i * 4)) - 16;
         int tileNum = this.cpu.addressRead(0xFE02 + (i * 4));
         int attributes = this.cpu.addressRead(0xFE03 + (i * 4));

         if ((attributes & 0x80) >> 7 == priority)
         {
            int spriteAttrib = 0;
            if (this.doubledSprites)
            {
               tileNum &= 0xFE;
            }
            vidRamAddress = tileNum << 4;
            if ((attributes & 0x10) != 0)
            {
               spriteAttrib |= TILE_OBJ2;
            }
            else
            {
               spriteAttrib |= TILE_OBJ1;
            }

            if ((attributes & 0x20) != 0)
            {
               spriteAttrib |= TILE_FLIPX;
            }
            if ((attributes & 0x40) != 0)
            {
               spriteAttrib |= TILE_FLIPY;
            }

            if (tiles[tileNum].invalid(spriteAttrib))
            {
               tiles[tileNum].validate(videoRam, vidRamAddress, spriteAttrib);
            }

            if ((spriteAttrib & TILE_FLIPY) != 0)
            {
               if (doubledSprites)
               {
                  this.tiles[tileNum].draw(back, spriteX, spriteY + 8, spriteAttrib);
               }
               else
               {
                  this.tiles[tileNum].draw(back, spriteX, spriteY, spriteAttrib);
	            }
            }
            else
            {
               this.tiles[tileNum].draw(back, spriteX, spriteY, spriteAttrib);
            }

            if (this.doubledSprites)
            {
               if (this.tiles[tileNum + 1].invalid(spriteAttrib))
               {
                  this.tiles[tileNum + 1].validate(videoRam, vidRamAddress + 16, spriteAttrib);
               }

               if ((spriteAttrib & TILE_FLIPY) != 0)
               {
                  this.tiles[tileNum + 1].draw(back, spriteX, spriteY, spriteAttrib);
               }
               else
               {
                  this.tiles[tileNum + 1].draw(back, spriteX, spriteY + 8, spriteAttrib);
               }
            }
         }
      }
   }

   /**
    * This must be called by the CPU for each scanline drawn by the display hardware. It
    *  handles the drawing of the background layer
    */
   public void notifyScanline(int line)
   {
      if ((this.framesDrawn % this.frameSkip) != 0)
      {
         return;
      }

      if (line == 0)
      {
         clearFrameBuffer();
         drawSprites(this.backBuffer.getGraphics(), 1);
         this.spritesEnabledThisFrame = this.spritesEnabled;
         this.windowStopLine = 144;
         this.windowEnableThisLine = this.winEnabled;
      }

      // SpritesEnabledThisFrame should be true if sprites were ever on this frame
      if (this.spritesEnabled)
      {
         this.spritesEnabledThisFrame = true;
      }
      
      if (this.windowEnableThisLine)
      {
         if (!this.winEnabled)
         {
            this.windowStopLine = line;
	         this.windowEnableThisLine = false;
         }
      }

      // Fix to screwed up status bars.  Record which data area is selected on the
      // first line the window is to be displayed.  Will work unless this is changed
      // after window is started
      // NOTE: Still no real support for hblank effects on window/sprites
      if (line == JayBoy.unsign(this.cpu.ioHandler.registers[0x4A]) + 1)
      {		// Compare against WY reg
         this.savedWindowDataSelect = this.bgWindowDataSelect;
      }

      // Can't disable background on GBC (?!).  Apperently not, according to BGB
      if ((!this.bgEnabled) && (!this.cpu.gbcFeatures))
      {
         return;
      }

      int xPixelOfs = JayBoy.unsign(this.cpu.ioHandler.registers[0x43]) % 8;
      int yPixelOfs = JayBoy.unsign(this.cpu.ioHandler.registers[0x42]) % 8;


      if ( ((yPixelOfs + line) % 8 == 4) || (line == 0))
      {
         if ((line >= 144) && (line < 152))
         {
            notifyScanline(line + 8);
         }

         Graphics back = this.backBuffer.getGraphics();

         int xTileOfs = JayBoy.unsign(this.cpu.ioHandler.registers[0x43]) / 8;
         int yTileOfs = JayBoy.unsign(this.cpu.ioHandler.registers[0x42]) / 8;
         int bgStartAddress, tileNum;

         int y = ((line + yPixelOfs) / 8);


         if (this.hiBgTileMapAddress)
         {
            bgStartAddress = 0x1C00;  /* 1C00 */
         }
         else
         {
            bgStartAddress = 0x1800;
         }

         int tileNumAddress, attributeData, vidMemAddr;

         for (int x = 0; x < 21; x++)
         {
            if (this.bgWindowDataSelect)
            {
               tileNumAddress = bgStartAddress + (((y + yTileOfs) % 32) * 32) + ((x + xTileOfs) % 32);
               tileNum = JayBoy.unsign(this.videoRam[tileNumAddress]);
               attributeData = JayBoy.unsign(this.videoRam[tileNumAddress + 0x2000]);
            }
            else
            {
               tileNumAddress = bgStartAddress + (((y + yTileOfs) % 32) * 32) + ((x + xTileOfs) % 32);
               tileNum = 256 + this.videoRam[tileNumAddress];
               attributeData = JayBoy.unsign(this.videoRam[tileNumAddress + 0x2000]);
            }

            int attribs = 0;

            vidMemAddr = (tileNum << 4);
            attribs = TILE_BKG;

            if (this.tiles[tileNum].invalid(attribs))
            {
               this.tiles[tileNum].validate(videoRam, vidMemAddr, attribs);
            }
            this.tiles[tileNum].
            draw(back, (8 * x) - xPixelOfs, (8 * y) - yPixelOfs, attribs);
         }
      }
   }

   /**
    * Clears the frame buffer to the background colour
    */
   public void clearFrameBuffer()
   {
      Graphics back = this.backBuffer.getGraphics();
      back.setColor(new Color(this.backgroundPalette.getRgbEntry(0)));
      back.fillRect(0, 0, 160 * this.mag, 144 * this.mag);
   }

   public boolean isFrameReady()
   {
      return (this.framesDrawn % this.frameSkip) == 0;
   }

   /**
    * Draw the current graphics frame into the given graphics context
    */
   public boolean draw(Graphics g, int startX, int startY, Component a)
   {
      int tileNum;
      calculateFPS();
      if ((this.framesDrawn % this.frameSkip) != 0)
      {
         this.frameDone = true;
         this.framesDrawn++;
         return false;
      }
      else
      {
         this.framesDrawn++;
      }
      Graphics back = this.backBuffer.getGraphics();

      /* Draw window */
      if (this.winEnabled)
      {
         int wx, wy;
         int windowStartAddress;

         if ((this.cpu.ioHandler.registers[0x40] & 0x40) != 0)
         {
            windowStartAddress = 0x1C00;
         } 
         else
         {
            windowStartAddress = 0x1800;
         }
         wx = JayBoy.unsign(this.cpu.ioHandler.registers[0x4B]) - 7;
         wy = JayBoy.unsign(this.cpu.ioHandler.registers[0x4A]);

         back.setColor(new Color(this.backgroundPalette.getRgbEntry(0)));
         back.fillRect(wx * this.mag, wy * this.mag, 160 * this.mag, 144 * this.mag);

         int tileAddress;
         int attribs, tileDataAddress;

         for (int y = 0; y < 19 - (wy / 8); y++)
         {
            for (int x = 0; x < 21 - (wx / 8); x++)
            {
               tileAddress = windowStartAddress + (y * 32) + x;
               if (!this.savedWindowDataSelect)
               {
                  tileNum = 256 + this.videoRam[tileAddress];
               }
               else
               {
                  tileNum = JayBoy.unsign(this.videoRam[tileAddress]);
               }
               tileDataAddress = tileNum << 4;

               attribs = TILE_BKG;

               if (wy + y * 8 < this.windowStopLine)
               {
                  if (this.tiles[tileNum].invalid(attribs))
                  {
                     this.tiles[tileNum].validate(this.videoRam, tileDataAddress, attribs);
                  }
                  this.tiles[tileNum].draw(back, wx + x * 8, wy + y * 8, attribs);
               }
	         }
         }
      }

      // Draw sprites if the flag was on at any time during this frame
      drawSprites(back, 0);
      if ((this.spritesEnabled) && (this.cpu.gbcFeatures))
      {
         drawSprites(back, 1);
      }

      g.drawImage(this.backBuffer, startX, startY, null);


      this.frameDone = true;
      return true;
   }


   /** This class represents a tile in the tile data area.  It
    * contains images for a tile in each of it's three palettes
    * and images that are flipped horizontally and vertically.
    * The images are only created when needed, by calling
    * updateImage().  They can then be drawn by calling draw().
    */
   class GameboyTile
   {
      private Image[] image = new Image[64];

      /**
       * True, if the tile's image in the image[] array is a valid representation of the tile as it
       * appers in video memory.
       * */
      private boolean[] valid = new boolean[64];

      private MemoryImageSource[] source = new MemoryImageSource[64];

      /** Current magnification value of Gameboy screen */
      private int magnify = 2;
      private int[] imageData = new int[64 * magnify * magnify];
      private Component a;

      /**
       * Intialize a new Gameboy tile
       */
      public GameboyTile(Component a)
      {
         allocateImage(TILE_BKG, a);
         this.a = a;
      }

      /**
       * Allocate memory for the tile image with the specified attributes
       */
      public void allocateImage(int attribs, Component a)
      {
         this.source[attribs] = new MemoryImageSource(8 * this.magnify, 8 * this.magnify,
         new DirectColorModel(32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000),
                              this.imageData, 0, 8 * this.magnify);
         this.source[attribs].setAnimated(true);
         this.image[attribs] = a.createImage(this.source[attribs]);
      }

      /**
       * Free memory used by this tile
       * */
      public void dispose()
      {                  
         for (int r = 0; r < 64; r++)
         {
            if (this.image[r] != null)
            {
               this.image[r].flush();
               this.valid[r] = false;
            }
         }
      }

      /**
       * Returns true if this tile does not contian a valid image for the tile with the specified
       * attributes
       */
      public boolean invalid(int attribs)
      {
         return (!this.valid[attribs]);
      }

      /** Create the image of a tile in the tile cache by reading the relevant data from video
       *  memory
       */
      public void updateImage(byte[] videoRam, int offset, int attribs)
      {
         int px, py;
         int rgbValue;

         if (this.image[attribs] == null)
         {
            allocateImage(attribs, a);
         }
      
         GameboyPalette pal;
      
         if (offset == 0x31E0)
         {         }
         if ((attribs & TILE_OBJ1) != 0)
         {
            pal = obj1Palette;
         }
         else if ((attribs & TILE_OBJ2) != 0)
         {
            pal = obj2Palette;
         }
         else
         {
            pal = backgroundPalette;
         }

         for (int y = 0; y < 8; y++)
         {
            for (int x = 0; x < 8; x++)
            {

               if ((attribs & TILE_FLIPX) != 0)
               {
                  px = 7 - x;
               }
               else
               {
                  px = x;
               }
               if ((attribs & TILE_FLIPY) != 0)
               {
                  py = 7 - y;
               }
               else
               {
                  py = y;
               }
            
               int pixelColorLower = (videoRam[offset + (py * 2)] & (0x80 >> px)) >> (7 - px);
               int pixelColorUpper = (videoRam[offset + (py * 2) + 1] & (0x80 >> px)) >> (7 - px);
            
               int entryNumber = (pixelColorUpper * 2) + pixelColorLower;
               int pixelColor = pal.getEntry(entryNumber);

               rgbValue = pal.getRgbEntry(entryNumber);

               /* Turn on transparency for background */

               if ((!cpu.gbcFeatures) || ((attribs >> 2) > 7))
               {
                  if (entryNumber == 0)
                  {
                     rgbValue &= 0x00FFFFFF;
                  }
               }

               for (int cy = 0; cy < this.magnify; cy++)
               {
                  for (int cx = 0; cx < this.magnify; cx++)
                  {
                     imageData[(y * 8 * this.magnify * this.magnify) + (cy * 8 * this.magnify) +
                              (x * this.magnify) + cx] = rgbValue;
                  }
               }
            }
         }
         this.source[attribs].newPixels();
         this.valid[attribs] = true;
      }

      /**
       * Draw the tile with the specified attributes into the graphics context given
       */
      public void draw(Graphics g, int x, int y, int attribs)
      {
         g.drawImage(this.image[attribs], x * this.magnify, y * this.magnify, null);
      }

      /**
       * Ensure that the tile is valid
       */
      public void validate(byte[] videoRam, int offset, int attribs)
      {
         if (!this.valid[attribs])
         {
            updateImage(videoRam, offset, attribs);
         }
      }

      /**
       * Change the magnification of the tile
       */
      public void setMagnify(int m)
      {
         for (int r = 0; r < 64; r++)
         {
            this.valid[r] = false;
            this.source[r] = null;
            if (image[r] != null)
            {
               this.image[r].flush();
               this.image[r] = null;
            }
         }
         this.magnify = m;
         this.imageData = new int[64 * this.magnify * this.magnify];
      }

      /**
       * Invalidate tile with the specified palette, including all flipped versions.
       */
      public void invalidate(int attribs)
      {
         this.valid[attribs] = false;       /* Invalidate original image and */
         if (this.image[attribs] != null) this.image[attribs].flush();

         this.valid[attribs + 1] = false;   /* all flipped versions in cache */
         if (this.image[attribs + 1] != null) this.image[attribs + 1].flush();

         this.valid[attribs + 2] = false;
         if (this.image[attribs + 2] != null) this.image[attribs + 2].flush();
         
         this.valid[attribs + 3] = false;
         if (this.image[attribs + 3] != null) this.image[attribs + 3].flush();
      }

      /**
       * Invalidate this tile
       */
      public void invalidate()
      {
         for (int r = 0; r < 64; r++)
         {
            this.valid[r] = false;
            if (this.image[r] != null) this.image[r].flush();
            this.image[r] = null;
         }
      }
   }
}
