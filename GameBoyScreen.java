import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ComponentListener;
import java.awt.event.ItemListener;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;

/** This class is used when JayBoy is run as an application
 *  to provide the user interface.
 */

class GameBoyScreen extends Frame implements ActionListener, ComponentListener, ItemListener
{
  /** referencing current JayBoy. */
  JayBoy applet;

  CheckboxMenuItem viewFrameCounter;

  /** Creates the JayBoy interface, with the specified title text */
  public GameBoyScreen(String s, JayBoy a)
  {
    super(s); //creating aframe (window)
    this.applet = a;
    setWindowSize(2);

    this.addComponentListener(this);

    MenuBar menuBar = new MenuBar(); //creating a new menu bar

    MenuItem fileOpen = new MenuItem("Open ROM"); //adding a new item
    fileOpen.setActionCommand("Open ROM");
    fileOpen.addActionListener(this);

    MenuItem fileEmulate = new MenuItem("Emulate"); //adding a new item
    fileEmulate.setActionCommand("Emulate");
    fileEmulate.addActionListener(this);

    MenuItem fileReset = new MenuItem("Reset"); //adding a new item
    fileReset.setActionCommand("Reset");
    fileReset.addActionListener(this);

    MenuItem filePause = new MenuItem("Pause"); //adding a new item
    filePause.setActionCommand("Pause");
    filePause.addActionListener(this);

    MenuItem fileQuit = new MenuItem("Exit"); //adding a new item
    fileQuit.setActionCommand("Exit");
    fileQuit.addActionListener(this);

    this.viewFrameCounter = new CheckboxMenuItem("Frame counter"); //adding a new item
    this.viewFrameCounter.setActionCommand("Frame counter");
    this.viewFrameCounter.addActionListener(this);

    MenuItem debugEnter = new MenuItem("Enter debugger"); //adding a new item
    debugEnter.setActionCommand("Enter debugger");
    debugEnter.addActionListener(this);



    Menu fileMenu = new Menu("File"); //creating a file menu
    Menu viewMenu = new Menu("View"); //creating a view menu
    Menu debugMenu = new Menu("Debug"); //creating a debug menu

    fileMenu.add(fileOpen); //adding to the file menu
    fileMenu.add(fileReset);
    fileMenu.add(filePause);
    fileMenu.add(fileEmulate);
    fileMenu.add(new MenuItem("-"));
    fileMenu.add(fileQuit);

    viewMenu.add(this.viewFrameCounter); //adding to the view menu


    debugMenu.add(debugEnter); //adding to the debug menu

    menuBar.add(fileMenu); //adding all menus to the menubar
    menuBar.add(viewMenu);
    menuBar.add(debugMenu);

    setMenuBar(menuBar); //setting the menubar
  }

  public void update(Graphics g)
  {
    paint(g);
  }

  /** Clear the frame to white */
  public void clearWindow()
  {
    Dimension d = getSize();
    Graphics g = getGraphics();
    g.setColor(new Color(255, 255, 255)); //white
    g.fillRect(0, 0, d.width, d.height);
  }

  public void componentHidden(ComponentEvent e) {
  }

  public void componentMoved(ComponentEvent e) {
  }

  public void componentResized(ComponentEvent e)
  {
    clearWindow();
  }              

  public void componentShown(ComponentEvent e) {
  }

  /** Resize the Frame to a suitable size for a Gameboy with a magnification given */
  public void setWindowSize(int mag)
  {
    setSize(175 * mag + 20, 174 * mag + 20);
  }

  /**
   * performs a requested action
   */
  public void actionPerformed(ActionEvent e)
  {
    String command = e.getActionCommand();

    if (command.equals("Open ROM"))
    {
      if (this.applet.cpu != null)
      {
        this.applet.cpu.terminate = true; //terminate the cpu
        if (this.applet.cartridge != null)
        {
          this.applet.cartridge.dispose(); //dispose of current cartridge if it exists
        }
        if (this.applet.cpu != null)
        {
          this.applet.cpu.dispose(); //disposing of current cpu
          this.applet.cpu = null;
        }
        clearWindow(); //clear window
      }

      FileDialog fd = new FileDialog(this, "Open ROM"); //open a file dialog window
      fd.setVisible(true);

      if (fd.getFile() != null) //if a file was selected:
      {
        this.applet.cartridge = new Cartridge(fd.getDirectory() + fd.getFile()); //create a new cartridge with the given rom
        this.applet.cpu = new Cpu(this.applet.cartridge, this); //create a new cpu with the new cartridge
        this.applet.cpu.graphicsChip.setMagnify(2);
        this.applet.cpu.reset(); //resetting the cpu
      }

    }
    else if (command.equals("Frame counter"))
    {
      this.viewFrameCounter.setState(!this.viewFrameCounter.getState()); //enabling or disabling the view of the FPS
    }
    else if (command.equals("Emulate"))
    {
      if ((this.applet.cartridge != null) && (this.applet.cartridge.cartridgeReady)) //if everything is ready
      {
        this.applet.queueDebuggerCommand("g"); //execute forever
        this.applet.cpu.terminate = true;
      }
      else
      {
        JayBoy.debugLog("Error - You need to load a ROM before you select 'Emulate'.");    
      }
    }
    else if (command.equals("Reset"))
    {
      this.applet.queueDebuggerCommand("s;g"); //reset the cpu and execute forever
      this.applet.cpu.terminate = true;
    }
    else if (command.equals("Pause"))
    {
      this.applet.cpu.terminate = true; //terminate the execution process
    }
    else if (command.equals("Enter debugger"))
    {
      if (this.applet.cpu != null)
      {
        this.applet.setDebuggerActive(true); //activating the debugger
        this.applet.cpu.terminate = true; //terminating the execution process
      }
      else
      {
        JayBoy.debugLog("Error - Load a ROM before entering the debugger");
      }
    }
    else if (command.equals("Exit"))
    {
      this.applet.dispose(); //disposees of everything and exiting
      System.exit(0);
    } 
  }


  public void itemStateChanged(ItemEvent e)
  { //has to be implemented...
    String command = (String) e.getItem();
    System.out.println(command);
  }

  public void paint(Graphics g)
  {
    if (this.applet.cpu != null && this.applet.cpu.graphicsChip != null)
    {
      Dimension d = getSize();
      int x = (d.width / 2) - (this.applet.cpu.graphicsChip.width / 2);
      int y = (d.height / 2) - (this.applet.cpu.graphicsChip.height / 2);
      boolean b = this.applet.cpu.graphicsChip.draw(g, x, y + 20, this); //draw stuff
      if (this.viewFrameCounter.getState())
      { //drawing the frame counter
        g.setColor(new Color(255, 255, 255));
        g.fillRect(0, d.height - 20, d.width, 20);
        g.setColor(new Color(0, 0, 0));
        g.drawString(this.applet.cpu.graphicsChip.getFPS() + " frames per second", 10, d.height - 7);
      }
    }
  }
}

