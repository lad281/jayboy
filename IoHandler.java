/** This class handles all the memory mapped IO in the range
 *  FF00 - FF4F.  It also handles high memory accessed by the
 *  LDH instruction which is locted at FF50 - FFFF.
 */
class IoHandler
{

  /** Data contained in the handled memory area */
  byte[] registers = new byte[0x100];

  /** Reference to the current CPU object */
  Cpu cpu;

  /** Current state of the button, true = pressed. */
  boolean padLeft, padRight, padUp, padDown, padA, padB, padStart, padSelect;

  /** Create an IoHandler for the specified CPU */
  public IoHandler(Cpu d)
  {
    this.cpu = d;
    reset();
  }

  /** Initialize IO to initial power on state */
  public void reset()
  {
    System.out.println("Hardware reset");
    for (int r = 0; r < 0xFF; r++)
    {
      ioWrite(r, (short) 0x00);
    }
    ioWrite(0x40, (short) 0x91);
    ioWrite(0x0F, (short) 0x01);
  }

  /** Press/release a Gameboy button by name */
  public void toggleKey(String keyName)
  {
    if (keyName.equals("a"))
    {
      this.padA = !this.padA;
      System.out.println("- A is now " + this.padA);
    }
    else if (keyName.equals("b"))
    {
      this.padB = !this.padB;
      System.out.println("- B is now " + this.padB);
    }
    else if (keyName.equals("up"))
    {
      this.padUp = !this.padUp;
      System.out.println("- Up is now " + this.padUp);
    }
    else if (keyName.equals("down"))
    {
      this.padDown = !this.padDown;
      System.out.println("- Down is now " + this.padDown);
    }
    else if (keyName.equals("left"))
    {
      this.padLeft = !this.padLeft;
      System.out.println("- Left is now " + this.padLeft);
    }
    else if (keyName.equals("right"))
    {
      this.padRight = !this.padRight;
      System.out.println("- Right is now " + this.padRight);
    }
    else if (keyName.equals("select"))
    {
      this.padSelect = !this.padSelect;
      System.out.println("- Select is now " + this.padSelect);
    }
    else if (keyName.equals("start"))
    {
      this.padStart = !this.padStart;
      System.out.println("- Start is now " + this.padStart);
    }
    else
    {
      System.out.println("- Key name '" + keyName + "' not recognised");
    }
  }

  /** Read data from IO Ram */
  public short ioRead(int num)
  {
    if (num <= 0x4B)
    {
    }
   
    switch (num)
    {
      case 0x41 :         // LCDSTAT
        int output = 0;

        if (this.registers[0x44] == this.registers[0x45])
        {
          output |= 4;
        }
     
        int cyclePos = this.cpu.getInstrCount() % this.cpu.INSTRS_PER_HBLANK;
        int sectionLength = this.cpu.INSTRS_PER_HBLANK / 6;
        
        if (JayBoy.unsign(this.registers[0x44]) > 144)
        {
          output |= 1;
        }
        else
        {
          if (cyclePos <= sectionLength * 3)
          {
            // Mode 0
          }
          else if (cyclePos <= sectionLength * 4)
          {
            // Mode 2
            output |= 2;
          }
          else
          {
          output |= 3;
          }
        }
     
        // System.out.println("Checking LCDSTAT");
        return (byte) (output | (this.registers[0x41] & 0xF8));
     
      case 0x55:
        return (byte) (this.registers[0x55]);
     
      case 0x69 :       // gb color BG Sprite palette
        return this.registers[num];
     
      case 0x6B :     // gb color OBJ Sprite palette
        return this.registers[num];

      default:
        return this.registers[num];
    }
  }

  /** Write data to IO Ram */
  public void ioWrite(int num, short data)
  {
    if (num <= 0x4B)
    {
    }

    switch (num)
    {
      case 0x00 :           // FF00 - Joypad
        short output = 0x0F;
        if ((data & 0x10) == 0x00)
        {   // P14
          if (this.padRight)
          {
            output &= ~1;
          }
          if (this.padLeft)
          {
            output &= ~2;
          }
          if (padUp)
          {
            output &= ~4;
          }
          if (this.padDown)
          {
            output &= ~8;
          }
        }
        if ((data & 0x20) == 0x00)
        {   // P15
          if (this.padA)
          {
            output &= ~0x01;
          }
          if (this.padB)
          {
            output &= ~0x02;
          }
          if (this.padSelect)
          {
            output &= ~0x04;
          }
          if (this.padStart)
          {
            output &= ~0x08;
          }
        }
	      output |= (data & 0xF0);
        this.registers[0x00] = (byte) (output);
        break;

      case 0x02 :           // Serial
        this.registers[0x02] = (byte) data;
        break;

      case 0x04 :     // DIV
        this.registers[04] = 0;
        break;    

      case 0x07 :     // TAC
        if ((data & 0x04) == 0)
        {
          this.cpu.timaEnabled = false;
        }
        else
        {
          this.cpu.timaEnabled = true;
        }

        int instrsPerSecond = this.cpu.INSTRS_PER_VBLANK * 60;
        int clockFrequency = (data & 0x03);

        switch (clockFrequency)
        {
          case 0: this.cpu.instrsPerTima = (instrsPerSecond / 4096);
                  break;
          case 1: this.cpu.instrsPerTima = (instrsPerSecond / 262144);
                  break;
          case 2: this.cpu.instrsPerTima = (instrsPerSecond / 65536);
                  break;
          case 3: this.cpu.instrsPerTima = (instrsPerSecond / 16384);
                  break;
        }
        break;

      case 0x10 :     // Sound channel 1, sweep
        this.registers[0x10] = (byte) data;
        break;

      case 0x11 :      // Sound channel 1, length and wave duty
        this.registers[0x11] = (byte) data;
        break;

      case 0x12 :     // Sound channel 1, volume envelope
        this.registers[0x12] = (byte) data;
        break;

      case 0x13 :    // Sound channel 1, frequency low
        this.registers[0x13] = (byte) data;
        break;

      case 0x14 :           // Sound channel 1, frequency high
        this.registers[0x14] = (byte) data;
        break;

      case 0x17 :     // Sound channel 2, volume envelope
        this.registers[0x17] = (byte) data;
        break;

      case 0x18 :    // Sound channel 2, frequency low
        this.registers[0x18] = (byte) data;
        break;

      case 0x19 :    // Sound channel 2, frequency high
        this.registers[0x19] = (byte) data;
        break;

      case 0x16 :    // Sound channel 2, length and wave duty
        this.registers[0x16] = (byte) data;
        break;

      case 0x1A :    // Sound channel 3, on/off
        this.registers[0x1A] = (byte) data;
        break;

      case 0x1B :    // Sound channel 3, length
        this.registers[0x1B] = (byte) data;
        break;

      case 0x1C :   // Sound channel 3, volume
        this.registers[0x1C] = (byte) data;
        break;

      case 0x1D :   // Sound channel 3, frequency lower 8-bit
        this.registers[0x1D] = (byte) data;
        break;

      case 0x1E :    // Sound channel 3, frequency higher 3-bit
        this.registers[0x1E] = (byte) data;
        break;

      case 0x20 :  // Sound channel 4, length
        this.registers[0x20] = (byte) data;
        break;


      case 0x21 :     // Sound channel 4, volume envelope
        this.registers[0x21] = (byte) data;
        break;

      case 0x22 :           // Sound channel 4, polynomial parameters
	      this.registers[0x22] = (byte) data;
        break;

      case 0x23 :          // Sound channel 4, initial/consecutive
        this.registers[0x23] = (byte) data;
        break;

      case 0x25 :    // Stereo select
        this.registers[0x25] = (byte) data;
        break;

      case 0x30 :
      case 0x31 :
      case 0x32 :
      case 0x33 :
      case 0x34 :
      case 0x35 :
      case 0x36 :
      case 0x37 :
      case 0x38 :
      case 0x39 :
      case 0x3A :
      case 0x3B :
      case 0x3C :
      case 0x3D :
      case 0x3E :
      case 0x3F :
        this.registers[num] = (byte) data;
        break;


      case 0x40 :           // LCDC
        this.cpu.graphicsChip.bgEnabled = true;

        if ((data & 0x20) == 0x20)
        {     // BIT 5
          this.cpu.graphicsChip.winEnabled = true;
        }
        else
        {
          this.cpu.graphicsChip.winEnabled = false;
        }

        if ((data & 0x10) == 0x10)
        {     // BIT 4
          this.cpu.graphicsChip.bgWindowDataSelect = true;
        }
        else
        {
          this.cpu.graphicsChip.bgWindowDataSelect = false;
        }

        if ((data & 0x08) == 0x08)
        {
          this.cpu.graphicsChip.hiBgTileMapAddress = true;
        }
        else
        {
          this.cpu.graphicsChip.hiBgTileMapAddress = false;
        }

        if ((data & 0x04) == 0x04)
        {      // BIT 2
          this.cpu.graphicsChip.doubledSprites = true;
        }
        else
        {
          this.cpu.graphicsChip.doubledSprites = false;
        }

        if ((data & 0x02) == 0x02)
        {     // BIT 1
          this.cpu.graphicsChip.spritesEnabled = true;
        }
        else
        {
          this.cpu.graphicsChip.spritesEnabled = false;
        }

        if ((data & 0x01) == 0x00)
        {     // BIT 0
          this.cpu.graphicsChip.bgEnabled = false;
          this.cpu.graphicsChip.winEnabled = false;
        }

        this.registers[0x40] = (byte) data;
        break;

      case 0x41 :
	      this.registers[0x41] = (byte) data;
	      break;

      case 0x42 :           // SCY
        this.registers[0x42] = (byte) data;
        break;

      case 0x43 :           // SCX
        this.registers[0x43] = (byte) data;
        break;

      case 0x46 :           // DMA
        int sourceAddress = (data << 8);

        for (int i = 0x00; i < 0xA0; i++)
        {
          this.cpu.addressWrite(0xFE00 + i, this.cpu.addressRead(sourceAddress + i));
        }
        // This is meant to be run at the same time as the CPU is executing
        // instructions, but it's not crucial.
        break;

      case 0x47 :           // FF47 - BKG and WIN palette
        //    System.out.println("Palette created!");
        this.cpu.graphicsChip.backgroundPalette.decodePalette(data);
        if (this.registers[num] != (byte) data)
        {
          this.registers[num] = (byte) data;
          this.cpu.graphicsChip.invalidateAll(GraphicsChip.TILE_BKG);
        }
        break;

      case 0x48 :           // FF48 - OBJ1 palette
        this.cpu.graphicsChip.obj1Palette.decodePalette(data);
        if (this.registers[num] != (byte) data)
        {
          this.registers[num] = (byte) data;
          this.cpu.graphicsChip.invalidateAll(GraphicsChip.TILE_OBJ1);
        }
        break;

      case 0x49 :           // FF49 - OBJ2 palette
        this.cpu.graphicsChip.obj2Palette.decodePalette(data);
        if (this.registers[num] != (byte) data)
        {
          this.registers[num] = (byte) data;
          this.cpu.graphicsChip.invalidateAll(GraphicsChip.TILE_OBJ2);
        }
        break;

      case 0x4F :
        this.registers[0x4F] = (byte) data;
        break;


      case 0x55 :
        if (((this.registers[0x55] & 0x80) == 0) && ((data & 0x80) == 0))
        {
          int dmaSrc = (JayBoy.unsign(this.registers[0x51]) << 8) +
                        (JayBoy.unsign(this.registers[0x52]) & 0xF0);
          int dmaDst = ((JayBoy.unsign(this.registers[0x53]) & 0x1F) << 8) +
                        (JayBoy.unsign(this.registers[0x54]) & 0xF0) + 0x8000;
          int dmaLen = ((JayBoy.unsign(data) & 0x7F) * 16) + 16;

          if (dmaLen > 2048) dmaLen = 2048;

          for (int r = 0; r < dmaLen; r++)
          {
            this.cpu.addressWrite(dmaDst + r, this.cpu.addressRead(dmaSrc + r));
          }
        }
        this.registers[0x55] = (byte) data;
        break;

      case 0x69 :           // FF69 - BCPD: gb color BG Palette data write
        break;

      case 0x6B :           // FF6B - OCPD: gb color Sprite Palette data write
        break;

      case 0x70 :           // FF70 - gb color Work RAM bank
        break;

      default:
        this.registers[num] = (byte) data;
        break;
    }
  }
}

